# 2022-06-08 Brainstorming with Yann Vonlanthen

## GitDao v1

Use an exponentially decreasing function, i.e. divide all token's power by the same number at each time intervals.
This preserves proportions between token holders which is desirable.
If the last person to get a token get more power over time, this is bad for decentralization!

      a      b       c         d
t0  ---------------------------------- (power owned at time t0 by a, b, c and d is the entire height of the graph)
    |     |       |       |          |
    |     |       |       |          |
t1  ---------------------------------- (at time t1, the power of everyone is halved)
    |     |       |       |          |
t2  ---------------------------------- (at time t2, the power of everyone is again halved)
    |     |       |       |          |
    ----------------------------------

We remark that everyone keeps the same proportional power.

## GitDao v2

Implement two classes of citizens:

- Devz: They participate actively in the creation of the product and receive ERC721 token (as described in GitDao v1?)
- Stakeholderz: They invest and take part in the governance. They receive ERC20 tokens.

Use an evolutionist approach to determine the kind of governance to setup?
SARL pour les stakeholderz, system bichaméral comme dans beaucoup de gouvernements entre devz et stakeholderz.

### ERC20 Minting

- Mint when investors invest in the DAO.
  The money invested goes to the DAO treasury and the DAO can do whatever with it.
- Mint when token holders (devz or stakeholderz) participate in the DAO governance.
  Incentivizes active participation.
  Creates some inflation on the token, which incentivizes people to participate even more.
  Rewards for active participation needs to be proportional to the amount of token used for sybil resistance (though it would be better for decentralization that the amount is fixed per participant).

Liquidity pools will be created for the ERC20 tokens, so they will also have a market price.
-> Define at which price the tokens are minted! This sets an upper bound on the value the tokens can have...
=> Think about this aspect and analyze in details!

### Feature Request

People can request features.
They stake some money on a feature, and get the feature in exchange for their money (no ERC20 tokens).
Money is staked for a limitted amount of time during which the feature has to be completeed.
After that, and if no one implemented the feature, they get their money back.
If people want the feature fast, they have to stake more for shorter amounts of time.

## TODO

- Define how devz and stakeholderz interact, what are the responsibilities of each.
- Rewarding scheme can now get more complex by making the devz and the stakeholderz interact.
