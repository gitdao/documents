# Attacks Against open source

## Supply Chain Attacks

Found in https://github.com/crev-dev/crev/

- Rust, crates.io security breach: https://users.rust-lang.org/t/security-advisory-for-crates-io-2017-09-19/12960
- Ruby, RubyGems attacked: https://www.itnews.com.au/news/rubygems-in-recovery-mode-after-site-hack-330819
- Malicious Python libraries: https://www.zdnet.com/article/twelve-malicious-python-libraries-found-and-removed-from-pypi/
- Compromised ESLint: https://news.ycombinator.com/item?id=17513709
- Node.js corrupted from its dependencies: https://thenewstack.io/npm-attackers-sneak-a-backdoor-into-node-js-deployments-through-dependencies/
- Malicious packages in NPM: https://www.csoonline.com/article/3214624/malicious-code-in-the-node-js-npm-registry-shakes-open-source-trust-model.html

