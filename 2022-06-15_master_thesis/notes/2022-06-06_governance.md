---
title: DAO Governance
date: 2022-06-06
---

## open source

Open source code is a public good.

## GitDao Stakeholders

- Coders
- Designers
- Users of the project coordinated by the DAO
- Voters

## Informed Voters

For a code oriented DAO, we need people that understand what they are voting on.
However, even in a single project, the code base might be so large that no single individual is knowledgeable about every aspect of the system.
At scale, this generally implies that only a few individuals will be knowledgeable about a specific topic.

How can good voting occur in such circumstances?

Few technocrats, law of big numbers, disseminate knowledge to the masses?

Ideas to fix it:

1. Split up the voting into categories.
   First vote on which category a vote belongs to, then only ask people of this category to vote.
2. Liquid democracy.
   Delegate your vote to knowledgeable representatives.
3. Everyone still vote on everything, but before voting happens, foster debate by asking knowledgeable individuals to hold presentations/write articles about which solution they think is better.

Solution 1 and 2 cause more centralization.
Solution 3 is very appealing to me: it looks a lot like the Swiss democracy.

## Selecting Voters

Every voting system requires sybil resistance!

How are voters selected?

- Centrally selected voters (like badge holders of retrofunding round 1 by Optimism).
- Sybil resistance through token voting.
- Proof-of-humanity.

## Selecting Voter at Random

To fight voter fatigue in large-scale system.

## Good Mechanism Design

> Good mechanism design dictates that the overall organization be constructed such that rational actors vote truthfully in accordance with their estimates of the expected value of each proposal.
>
> (https://hackingdistributed.com/2016/05/27/dao-call-for-moratorium/)

## Problems with Token Votes

- Low voter participation
- Bribing can lead to attacks
- Non-representativeness, i.e. people holding tokens can be holding token for varying reasons (store-of-value, medium-of-exchange, etc.)
- Centralization: if voters are not technically knowledgeable and simply defer to a single dominant tribe of experts, you end up with centralized dictators.
  For example, this is what happens with liquid democracy, except that it is considered a feature there...

## Multifactorial Consensus

> The rookie mistake is: you see that some system is partly Moloch [ie. captured by misaligned special interests], so you say "Okay, we'll fix that by putting it under the control of this other system. And we'll control this other system by writing ‘DO NOT BECOME MOLOCH' on it in bright red marker."
> ("I see capitalism sometimes gets misaligned. Let's fix it by putting it under control of the government. We'll control the government by having only virtuous people in high offices.")
> I'm not going to claim there's a great alternative, but the occasionally-adequate alternative is the neoliberal one – find a couple of elegant systems that all optimize along different criteria approximately aligned with human happiness, pit them off against each other in a structure of checks and balances, hope they screw up in different places like in that swiss cheese model, keep enough individual free choice around that people can exit any system that gets too terrible, and let cultural evolution do the rest.
>
> — slatestarcodex (http://slatestarcodex.com/2017/11/21/contra-robinson-on-public-food/)

> The approach for blockchain governance that I advocate is "multifactorial consensus", where different coordination flags and different mechanisms and groups are polled, and the ultimate decision depends on the collective result of all of these mechanisms together. These coordination flags may include:
>
> - The roadmap (ie. the set of ideas broadcasted earlier on in the project's history about the direction the project would be going)
> - Consensus among the dominant core development teams
> - Coin holder votes
> - User votes, through some kind of sybil-resistant polling system
> - Established norms (eg. non-interference with applications, the 21 million coin limit)
>
> — Vitalik Butterin (https://vitalik.ca/general/2017/12/17/voting.html)

Voter fatigue is probably an even larger problem in such a case.

## Rewarding Scheme

What is the risk profile of building open source code?
This should be used to define how people are remunerated.
Is it similar to startups (high risk for the individuals that take part in them), in which case we need a rewarding scheme that remunerate early contributors a lot.
Should people be remunerated equally all along?
How should people be remunerated if the project is very successful (many contributions from a large community, many many users)?
What about when the project is not successful (small community)?

Big advantage of coin voting: Sybil resistant!
How can we have users vote in a Sybil resistant way?

## Means-testing: Voting not on the Value of a Project, But Rather on the Project's Need for Funding

Context: Retroactive funding.

- Would incentivize people more to work on public good projects.
- Requires more efforts to evaluate the needs of each project.
- Create an incentive for projects to appear under-funded.
- Introduces the question of the worthiness of a project to receive funding, instead of considering only how useful the project was so far.

Conclusion of Butterin: voting can be influenced by means-testing, but should not be too much.
Projects should be judged by their effect on the world first and foremost.

## Filtering Proposals in Large-scale Voting Systems

URL: https://vitalik.ca/general/2021/11/16/retro1.html

- Badge holder pre-approval: for a proposal to become visible, it must be approved by N badge holders (eg. N=3?).
  Any N badge holders could pre-approve any project; this is an anti-spam speed bump, not a gate-keeping sub-committee.
- Require proposers to provide more information about their proposal, justifying it and reducing the work badge holders need to do to go through it.
- Proposals have to specify a category (eg. "zero knowledge proofs", "games", "India"), and badge holders who had declared themselves experts in that category would review those proposals and forward them to a vote only if they chose the right category and pass a basic smell test.
- Proposing requires a deposit of 0.02 ETH. If your proposal gets 0 votes (alternatively: if your proposal is explicitly deemed to be "spam"), your deposit is lost.
- Proposing requires a proof-of-humanity ID, with a maximum of 3 proposals per human. If your proposals get 0 votes (alternatively: if any of your proposals is explicitly deemed to be "spam"), you can no longer submit proposals for a year (or you have to provide a deposit).

## Fostering Discussion Around Votes

URL: https://vitalik.ca/general/2021/11/16/retro1.html

- Badge holders could vote in advisors, who cannot vote but can speak. -> Similar to a political party in Switzerland.
- Badge holders could be required to explain their decisions, eg. writing a post or a paragraph for each project they made votes on. -> More voter fatigue.
- Consider compensating badge holders, either through an explicit fixed fee or through a norm that badge holders themselves who made exceptional contributions to discussion are eligible for rewards in future rounds.
- Add more discussion formats. If the number of badge holders increases and there are subgroups with different specialties, there could be more chat rooms and each of them could invite outsiders. Another option is to create a dedicated subreddit.

## Secret Balots

Secret ballots in democracies: in direct votes.
Public votes in democracies: in legislative and executive assemblies (hold representatives accountable). Juries are also such examples.

With public ballots, vote can be bought, i.e. a buyer can now check that you did vote like you were asked and pay you afterwards, e.g. lobbying.
More coercitive means can also be used to make someone vote a specific option.

However, secret ballots are more complex to setup and do not offer transparency.

-> There is no perfect solution.

## Quadratic Voting

Benefit: Less power to the rich, more power to the masses.

Drawback: Requires Sybil-resistance.

## Tyranny of the Majority

In system that require only a majority of votes, the majority can oppress the minority and pursue only its own objectives.
Such a system might leave 50% of its people unsatisfied and oppressed.

## Gitcoin Grants: Public Good Funding

### Stability VS Inclusivity

If give to the same projects in every round, hard for new projects to get included, hence bad public-good funding.
If what projects get each round varies too much, then too hard for projects to rely on Gitcoin grants as a funding mechanism.

### Popular Implies Attacked

Gitcoin grants have become popular and recognized, hence fraud is becoming more of an issue.
Requires *closed-source* fraud-detection algorithms.

## Moral Values

*Complexity of value* is the thesis that human values have high Kolmogorov complexity; that our preferences cannot be summed by a few simple rules, or compressed.

*Fragility of value* is the thesis that losing even a small part of the rules that make up our values could lead to unacceptable results, just like dialing nine out of ten phone digits correctly does not connect you to a person 90% similar to your friend.

## Coordination Failures as a Feature

> Coordination problems are everywhere in society and are often a bad thing - while it would be better for most people if the English language got rid of its highly complex and irregular spelling system and made a phonetic one, or if the United States switched to metric, or if we could immediately drop all prices and wages by ten percent in the event of a recession, in practice this requires everyone to agree on the switch at the same time, and this is often very very hard.
>
> — Vitalik Butterin (https://vitalik.ca/general/2017/05/08/coordination_problems.html)

Coordinating many people to change things is hard.
This is a benefit of blockchains: they are built to offer some security properties, and changing the blockchain protocol in any meaningful way to break those security properties is hard.
This is also a drawback in the case of unforseen bug or failure mode.
This is only true because there are many users checking the validity of the chain, i.e. the control over the blockchain is decentralized.
In the case of centralized actors, like PayPal, changing the rules of the game is easy.
