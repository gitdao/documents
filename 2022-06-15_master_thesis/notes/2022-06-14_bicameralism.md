# Bicameralism

Used by so many parliaments that I would say there must be something right about this system from a darwinistic point of view.

As of 2015, 40% of the world's national legislatures are bicameral, the remaining 60% are unicameral.

I guess that it only make sense to use such a system in the case that each chamber follows a different set of rules.
(One could imagine that there are two chambers that follow the same rules, but this seems weird.)

Often, the bicameral system will require a concurrent majority, i.e. a majority in each chamber for each topic.

The first chamber is often composed of people elected by the people directly.
The second chamber has been used for different purposes.
Generally speaking, it offers an opportunity to think a second time about proposed laws.

## Critics of Bicameralism

Makes meaningful reform much harder.
Increases risk of gridloack.

## Benefits of Bicameralism
Checks and balances prevent ill-considered legislation.

## Federal Systems

Federal states often use the second chamber for an equal representation of each constituent states so as to over-represent minorities.

## Aristocratic Systems

### UK

House of Commons, elected by the people.
House of Lords, appointed for life by the Queen upon recommendations from the Prime Minister, or hereditary memberships.
Contains mostly nobles, clergy and previous members of the House of Commons.

Decisions made in one house have to be agreed upon by the other house to pass.

## Unitary States

Generally in unitary states, the upper house is only responsible of scrutinizing and possibly vetoing proposals of the lower house.

### Italian Government

Exception to the above rule.
Two chambers, each has the same role.
Each chamber is composed in a different way.
The first is elected by the people on a national scale.
The second is elected on a regional basis.
This can lead to different majorities in the chambers.

Note that the government needs to win the confidence of both chambers.
This has caused some deadlock in the past.

