# Creating [the-git-dao.crypto]()

Bought the domain name on Unstoppable Domains.

Created a website on [fleek.co]() for IPFS hosting.
I considered [pinata.cloud]() as an alternative but their free plan is a little less attractive:

| Property | Fleek | Pinata   |
|----------|-------|----------|
| Storage  | 3GB   | 1GB      |
| Bandwidth| 50GB  | no limit |

I might want to buy a regular domain too and connect it to the IPFS content through [DNSLink](https://docs.ipfs.io/concepts/dnslink/#publish-content-path).
But let's start with the Unstoppable Domain/Fleek/IPFS setup.

Right, right, Fleek requires code hosted on GitHub.
This is annoying as I want to do things on GitLab (personal preferences, and GitLab has many more features!).
Let's see what Pinata has to offer.

I'll need to write commands myself, but I guess this is better for learning too.
Account on Pinata created.
Read the documentation.
Managed to create an API key and upload and pin a file to pinata.
Now the question is how do expand on this to upload entire folders from a GitLab Pipeline in a way that remains manageable?

The API from Pinata seems rather new and unstable.
Using a local IPFS node and adding pinata as a remote pining service might be a reasonable option.

Uploading files from a local nodes takes ages...
Not sure this is really the way to go.
Ah, I guess this is because I was not running the IPFS deamon.

Decision of the day: perform all my pipelines through nix.
Why? Repeatability of the builds, use the same code on the pipelines and on my personal computer, easiness to install dependencies (the docker container for IPFS does not contain curl and it is based on tinybox, so it is very annoying to install anything on it).

Now the problem is to write ONE good GitLab Pipeline skeleton that I will be able to reuse in the future.

## 2022-01-30

Finally found a way to write a nix package that builds my website!
