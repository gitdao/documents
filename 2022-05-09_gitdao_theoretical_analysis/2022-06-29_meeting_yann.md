# 2022-06-29 Meeting with Yann

## ERC20 Minting

Two possible strategies:

- Minting opened all the time.
  Requires a way to fix the minting price.
  Implies infinite supply.

- Funding rounds, i.e. minting opened only at certain time.
  DAO decides when to raise new funds?
  ERC20 is also a way to decentralize power: need to release enough of it to decentralize power, needs to make sure no whales.

  Liquidity pool for the token might exist (similar to publicly traded company).
  Need to be careful about how set price of new shares to mint.
  How does a publicly traded company raise funds?

Whichever approach is taken, it will require a lot of knowledge from devs which might prefer just coding!
This has to be taken into account.
Maybe those are decisions that could be taken using the people from the ERC20 chamber?

## Dev Rewards

Core VS Halo distinction?
Have classes of developers?
How do we distinguish them?
How do you become a core dev?

A priori (salaries) or a posteriori rewards?
Guaranteed a priori rewards (salaries) are hard: the DAO might not have strong guarantees on the liquidity it will own.
Do only a posteriori rewards as easier to evaluate value of contribution through ERC721 and ERC20 tokens.
Devs get money proportional to ERC721 each time DAO receives money.

ERC721 VS ERC20 tokens?
ERC20 have a monetary value.
ERC721 have an expected return and give some power.

Making purely ERC721-based rewards seems like a bad idea: what if no donations?
Might want to reward all contributions with both ERC721 for decentralized governance of the ERC721 chamber (enable liquid democracy?) and ERC20 for monetary value and decentralized governance of the ERC20 chamber.

Need a rewarding scheme, i.e. a way to determine how much of each tokens to give for a given contribution!

What each tokens bring to its owner.

|-----------------------------|
|            | ERC721 | ERC20 |
|-----------------------------|
| power      | yes    | yes   |
| drips      | yes    | no    |
| sell value | no     | yes   |
|-----------------------------|

## Where Does GitDao Get Money From

- Issue backing
- Investing (ERC20)
- Drips?
  Does it still make sense to have drips or have we fully turned Open Source projects into startups?
  If funding rounds are punctual, it might be that Drips still make some sense.
  A DAO driping some funds to another project, might be interested in taking part in the governance of that other project?

## Power Distribution Between the Two Chambers

ERC721 chambers is the active chamber.
Reason: most questions are technical and devs are the right people to manage them.

ERC20 chambers votes at fixed intervals to approve the rewards granted to devs.
They are the chamber the devs are somewhat responsible to.
This works like "assemblée des actionnaires" in today's companies.

## Issue Backing

A way for community to influence the roadmap.
Great idea IMO!
Feature long missed in Open Source.

How should it be decided that a feature is completed?

- Voted by GitDao members: they have a long term incentive to behave honestly, otherwise people will stop backing issues.
  Possible attack: Rug-pull by the developers: they decide to mark all issues as solved to get the money from all and then stop developing the product.
  Possible mitigation: ensure ownership of project is decentralized enough.
- NOT voted by stackers: once the feature is built, the DAO has an incentive to merge the code even if no money (work was already invested and better to make the product better anyways), the stacker must see the code to be convinced that issue solved, but then the stacker has incentive to copy the code and run it on its own and say that feature not fulfilled to not pay the feature.
- A third party?
  Need to find a neutral party that has incentive in saying the truth.
  Problem: how has an interest in doing that and is expected to be neutral and honest?
  Kleros?
  On-chain court system?
  More complexity!

How much money does the DAO get for a backed issue?
Proposal: amount of money DAO gets decreases over time.
I.e. the longer you wait, the less money you will get from the issue.
Creates incentive to solve feature as soon as possible.
From stacker's perspective: the sooner you get the feature the more it will cost you (which sounds about correct).
What function to use to describe the money that you get upon feature completion?
Can the backer decide the function to use?

Who gets the money from the backed issue?

- Just the developers that solved the issue?
  Creates competition between developers for backed issue.
  Seems like a rather bad idea.
- Reward all ERC721 holders.
  Seems about right: fosters collaboration between devs.

## Define Properties of Interest and Prove Them for GitDao

- Progressive decentralization of ownership
- Only fork when reasonable to fork.
  If 95% community wants A and 5% community wants not A, and assuming 5% have control, this is a coordination issue.
  This is not a reasonable case for a fork; the governance should be improved upon instead.
  If 5% wants A, 95% wants not A and not A is implemented, then 5% probably don't have enough manpower to maintain a fork so will not fly anyway.
  If 50% wants A, 50% wants not A, then fork might be reasonable.
  => Some proper analysis of when fork are the optimal choice would be interesting.
  GitDao should only make forking a reasonable choice when cannot do better fundamentally.
- Security (take some real-life example that failed)
  - Project continuity (taken over by malicious actor is bad)
- Stronger incentives when big client based (issue backing)

## Possible Attack: Fork

Now that Open Source projects are able to win some money, it might be an idea for some to fork the project, develop more rapidly and win the money instead, i.e. competition instead of collaboration in the case competition has higher expected returns.
Need to prove the collaboration always the more rational approach.
Decreasing ERC721 tokens are key!
They ensure that contributors from long ago do not receive too much and that new contributors are properly incentivized.

## To Discuss Next Time

Code reviews incentivization.
`crev`

