# Inspired from https://flyx.org/nix-flakes-latex/
#
# Complete list of available TeX packages: https://raw.githubusercontent.com/NixOS/nixpkgs/nixos-21.11/pkgs/tools/typesetting/tex/texlive/pkgs.nix
#
# Information on the latexmkrc file:
# - https://www.overleaf.com/learn/latex/Articles/An_introduction_to_Kpathsea_and_how_TeX_engines_search_for_files
# - https://www.overleaf.com/learn/latex/Questions/I_have_a_lot_of_.cls%2C_.sty%2C_.bst_files%2C_and_I_want_to_put_them_in_a_folder_to_keep_my_project_uncluttered._But_my    _project_is_not_finding_them_to_compile_correctly.
#
# PDFTex Fonts: Look for "collection-font" at https://raw.githubusercontent.com/NixOS/nixpkgs/nixos-21.11/pkgs/tools/typesetting/tex/texlive/pkgs.nix
# Font names: https://www.overleaf.com/learn/latex/Font_typefaces
# Command to get exact font name from package: `fc-scan $(nix eval "nixpkgs#lato.outPath" --raw) | grep family`
{
  description = "GitDao Analysis";
  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-utils.url = github:numtide/flake-utils;
    nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  };
  outputs = { self, devshell, flake-utils, nixpkgs }:
    with flake-utils.lib; eachSystem allSystems (system:
    let

      commands = {
        build = ''
            latexmk -interaction=nonstopmode -pdf -pdflatex \
              -pretex="\pdftrailerid{}" \
              -usepretex ${documentName}.tex
          '';
          # ''
          #   latexmk -interaction=nonstopmode -pdf -lualatex \
          #     -pretex="\pdfvariable suppressoptionalinfo 512\relax" \
          #     -usepretex ${documentName}.tex
          # '';

        clean = ''
          rm ${documentName}.aux ${documentName}.fdb_latexmk ${documentName}.fls ${documentName}.log ${documentName}.out
        '';

        prepare = ''
          mkdir -p .cache/texmf-var
        '';
      };

      documentName = "2022-05-09_gitdao_theoretical_analysis";

      env = [
        {
          name = "OSFONTDIR";
          value = builtins.concatStringsSep ":" (map (fontPackage: "${fontPackage}/share/fonts//") fonts);
        }
        { # To make reproducible document, we set the date to the latest commit.
          name = "SOURCE_DATE_EPOCH";
          value = toString self.lastModified;
        }
        { # Required by LuaLaTeX.
          name = "TEXMFHOME";
          value = ".cache";
        }
        { # Required by LuaLateX.
          name = "TEXMFVAR";
          value = ".cache/texmf-var";
        }
      ];
      envAsBashExports = builtins.concatStringsSep " " (pkgs.lib.attrsets.mapAttrsToList (name: value: "${name}=\"${value}\"") (builtins.listToAttrs env));

      fonts = [
        # pkgs.stdenvNoCC.mkDerivation {
        #   pname = "charlevoix-pro";
        #   version = "1.0.0";
        #   src = ./fonts/charlevoix-pro;
        #   phases = [ "unpackPhase" "installPhase" ];
        #   installPhase = ''
        #     mkdir -p $out/share/fonts/opentype
        #     cp -r . $out/share/fonts/opentype
        #   '';
        # }
      ];

      packagesForLatex = [
        pkgs.coreutils
        pkgs.gnused # Required by PDFLaTeX at least
        tex
      ] ++ fonts;

      pkgs = import nixpkgs {
        inherit system;
        overlays = [ devshell.overlay ];
      };

      # Complete list of available teX packages: https://raw.githubusercontent.com/NixOS/nixpkgs/nixos-21.11/pkgs/tools/typesetting/tex/texlive/pkgs.nix
      tex = pkgs.texlive.combine {
        inherit (pkgs.texlive)
        scheme-basic
        amsfonts
        csquotes
        float
        geometry
        graphicxpsd
        hyperref
        latex-bin
        latexmk
        microtype
        multicolrule
        pgf
        # spath3 # tikz library "calligraphy"
        times
        ;
      };
    in rec {
      devShell = # Used by `nix develop`, value needs to be a derivation
        pkgs.devshell.mkShell {
          name = documentName;

          inherit env;

          commands = [
            {
              name = "build";
              help = "Builds the document.";
              category = "LaTeX";
              command = let
                build = pkgs.writeScript "build" ''
                  ${commands.prepare}
                  ${commands.build}
                  clean
                '';
              in "${build}";
            }
            {
              name = "clean";
              help = "Does some cleanup in the folder.";
              category = "LaTeX";
              command = let
                build = pkgs.writeScript "clean" ''
                  ${commands.clean}
                '';
              in "${build}";
            }
          ];

          devshell.startup.prepareFolders.text = ''
            mkdir -p .cache/texmf-var
          '';

          packages = packagesForLatex;
        };

      packages = {
        document = pkgs.stdenvNoCC.mkDerivation rec {
          name = "document";
          src = ./.;
          buildInputs = packagesForLatex;
          phases = ["unpackPhase" "buildPhase" "installPhase"];
          buildPhase = ''
            export PATH="${pkgs.lib.makeBinPath buildInputs}";
            ${commands.prepare}
            env ${envAsBashExports} \
              ${commands.build}
          '';
          installPhase = ''
            mkdir -p $out
            cp ${documentName}.pdf $out/
          '';
        };
      };
      defaultPackage = packages.document;
    });
}
