% !TeX spellcheck = en_US
% !TeX program = pdflatex
\documentclass{article}

\usepackage{arxiv}
\usepackage{csquotes}
\usepackage{float}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{microtype}
\usepackage{multicol}
\usepackage{tikz}
\usetikzlibrary{%
	arrows,
	automata,
	calc,
	decorations,
	decorations.pathreplacing,
	fit,
	positioning,
	shapes.geometric
}

\newcommand{\gitdao}{\texttt{GitDaoV1}}

\newcommand{\median}[1]{\text{median}\left(#1\right)1)}
\newcommand{\weightedMedian}[1]{\textbf{median}\left(#1\right)}

\newlength{\x}%
\newlength{\y}%

% \usepackage[
% 	backend=biber,
% 	citestyle=numeric,
% 	style=numeric
% ]{biblatex}
% \bibliography{git_dao}

\author{%
	Yves \textsc{Zumbach}\\
	\texttt{yzumbach@student.ethz.ch}\\
	D-INFK, ETH Zürich
}
\title{GitDAO Workflows}
\subtitle{A Finite State Machine}

\begin{document}
	
	\maketitle
	
	\twocolumn
	
	\section{Properties of Interest}
	
	\subsection{To Think Some More}
	
	\begin{description}
		\item[Rewards for Completing Tasks]
			Offer one-time reward for completing tasks like designing a logo?
			Is this any use?
			Aren't there more appropriate tools today to do this off-chain?
		\item[User-driven Feaure Requests]
			Create a workflow for users to request features locking funds that are only unlocked once the feature is implemented.
			This can of course be used to request urgent security fixes.
		\item[Bug-Bounty Programs]
			Offer a bug-bounty program, i.e. anyone that finds a bug can claim a bounty from the DAO.
	\end{description}
	
	\subsection{Trivial}
	
	\begin{description}
		\item[Registry for Open Source projects]
			Upon creating a DAO for a project, or once the project is up and running, the DAO can reach out to a registry smart contract that list all existing open source projects to become listed as well.
			This is like GitHub, but on the blockchain.
		\item[Simplified Donation Workflow]
			Connecting open source projects to the blockchain makes it easy to donate.
			Additionally, if all projects are listed on a single registry, then donations are much easier to make as the same process applies to all projects.
			In opposition, today, most project do not offer an option to donate.
			The projects that do offer such an option, generally use different donation mechanisms making the experience heterogeneous, thus more complex for potential donors.
		\item[Transparency]
			Because all transactions are public on a blockchain, anyone can access the transactions from the DAO.
			This brings transparency over the project and where does the money go.
		\item[Decentralized Governance Process by Default]
			Move away from the Benevolent Dictator for Life Model, \textit{by default}.
			Prevents issues such as project maintained by a single individual that get bored after some time?
	\end{description}
	
	\subsection{Requires Game Theoretical Proof}
	
	\begin{description}
		\item[Voting?]
		\item[Long-term Support Guarantees?]
	\end{description}

	\section{\gitdao}
	
	This section specifies the working of the \gitdao, explain the reasons behind some design choices and the properties thus obtained.
	
	The \gitdao\ is a Decentralized Autonomous Organization (DAO), i.e. a system to coordinate a community around a common goal, in this case the betterment of a git-based code project.
	To coordinate the community supporting the project, the DAO feature, as is very common, a \textit{voting system}.
	To be able to vote, people need \textit{governance tokens} denoted $t_i$.

	\subsection{Context}

	We here give a few examples of contexts in which \gitdao\ could be used.

	\begin{itemize}
		\item
			A small open source project with a main contributors and some occasional contributors.
		\item
			A very large open source project like Python, which hundreds to thousands of developers, some very involved, some occasionally involved.
		\item
			A proprietary project in a company handled by a team of approximately twenty people.
	\end{itemize}

	It is good to keep in mind who the stakeholders can be when reading the analysis of the properties of \gitdao\ to make sure that the analysis holds in all these contexts.
	
	\subsection{Governance Tokens}
	
	Governance tokens of the \gitdao\ are \href{https://eips.ethereum.org/EIPS/eip-721}{ERC721} tokens featuring two main properties:

	\begin{enumerate}
		\item They are non-transferable.
		\item Each token's power decreases over time
	\end{enumerate}
	
	Governance tokens are non-transferable, i.e. any transfer function specified by the \href{https://eips.ethereum.org/EIPS/eip-721}{ERC721 standard} will yield an exception.
	It only makes sense to talk about the value of something if its value can be evaluated.
	If one can never receive or use an item, then this item will always have a zero value for me.
	Because tokens are non-transferable, they do not have a monetary value and it is not possible to create a market for them.
	This prevents people from contributing to the \gitdao\ as a bet on the future value of governance tokens.
	Further, tokens can only be obtained by contributing to the project in some meaningful way, e.g. by contributing some code.
	Thus, if you own some governance token, i.e.\ you have a stake in the project, then the tokens are the proof that you have invested time and energy in the project.
	
	Each token has a property called \textit{power}.
	Power is the \enquote{currency} that you can use to interact with the DAO.
	The power of each token is a decreasing function of time.
	This encodes that if you contributed a long time ago, and as projects evolve over time, then you are less involved now than you were then—compared for example to someone that contributed very recently.

	We propose to use a function linearly decreasing in time for the power of tokens.
	See figure~\ref{fig:token_power} for an illustration.
	Linear functions are easy to understand and predict.
	We believe that using them also lowers the potential for adversarial behaviors against the DAO.
	To illustrate this point we will consider using the exponentially decreasing function as a counter example.
	Assume a given \gitdao\ receives a large monthly donation at a fixed time.
	Because the exponentially decreasing function decreases rapidly around the origin, contributors will strongly prefer receiving new governance tokens right before the large donation.
	Note that the time required by a merge request (see section \ref{sec:merge_requests}) to reach either the \texttt{Succeeded} or the \texttt{Defeated} state is fixed and can be known in advanced.
	Thus some contributors might decide to wait until the optimal moment to submit their merge request which slows the overall development process.
	Using a linearly decreasing function limits the incentive to adopt such a behavior because the power of the token has the same derivative all the time.

	\begin{figure}[ht!]
		\centering
		\begin{tikzpicture}
			\draw [->] (0.5, 0) -- (0.5, 3) node [left] {power};
			\draw [->] (0.5, 0) -- (6, 0) node [below] {$t$};
			\draw (1, 2.5) -- (3, 0.5);
			\draw (3, 0.5) -- (5.5, 0.5);

			\fill (1, 2.5) circle (0.1);
			\draw [dashed] (1, 2.5) node [above, align=center] {Mint\\time} -- (1, 0) node [below] {$t_0$};
			\draw [dashed] (0.5, 2.5) node [left] {$p_{max}$} -- (1, 2.5);

			\fill (3, 0.5) circle (0.1);
			\draw [dashed] (0.5, 0.5) node [left] {$p_{min}$} -- (3, 0.5) node [above, align=center] {Minimum\\power reached};
			\draw [dashed] (3, 0.5) -- (3, 0) node [below] {$t_1$};

			\draw [decorate, decoration={brace, mirror, raise=6mm}] (1, 0) -- (3, 0) node [midway, below=7mm] {$\Delta t$};
			\draw [decorate, decoration={brace, raise=11mm}] (0.5, 0.5) -- (0.5, 2.5) node [midway, left=12mm] {$\Delta p$};
		\end{tikzpicture}
		\caption{%
			\label{fig:token_power}
			A potential function to describe the power of a token over time: the linear function.
			Its main advantages are that the function is very easy to anaylse and understand.
			Further, it limits the incentive that users might have to try to exploit the game.
		}
	\end{figure}

	Consider the creation of multiple governance token.
	Using linear functions, there are two reasonable design choices possible:

	\begin{description}
		\item[All tokens have the same $\Delta t$]
			All tokens will take the same time to go from $p_{max}$ to $p_{min}$, which implies that the power's derivative will depend on $\Delta p$.
		\item[All tokens have the same power derivative]
			In such a case, tokens with a greater initial power will take longer to reach $p_{min}$.
	\end{description}

	Note that the derivative of the power is given by:

	\[
		\text{power}'(t_i, t) = \frac{p_{min} - p_{max}}{t_1 - t_0} = \frac{\Delta p}{\Delta t}, t \in\ ]t_0, t_1[
	\]

	Tokens allow contributors to claim parts of the money that \gitdao\ receives.
	If all tokens have the same derivative, then people that contribute large features and thus get token with large initial power are remunerated twice: on one side they are rewarded with a high governance power, on the other side their power last longer.
	For this reason we decided that all token have the same $\Delta t$.
	This makes reasoning about contributor rewards easier.

	\textcolor{red}{Describe the esperance of the reward granted byu a token here using an integral!}

	We now turn our attention to possible values for $p_{min}$.
	There are two interesting cases:

	\begin{description}
		\item[$p_{min} > 0$]
			In such a case, getting a token means that, for all eternity, you have a say in the project and a claim on a part of the donations received by the DAO.
			Over time, and as more contributions are made to the project, the part you have a claim on will get smaller, this process is akin to inflation: a unit of governance will represent a smaller and smaller percentage of the donations over time.
			This can be compensated by increasing the power of the token over time.
			This makes the process more complex though.
			It also raises the question of whether $p_{min}$ should be an constant identical for all tokens or if it should be relative to $p_{max}$.
			If $p_{min}$ is a constant, then contributors have an incentive to break their contribution up in as many different merge requests as possible to increase their long-term power, which seems counter-productive: it creates more friction.
			This seems to indicate the $p_{min}$ should be defined as a fixed percentage of $p_{max}$ so as to retain the relative importance of contributions.
			We remark that this strategy incentivizes early contributions to the project as it gives you the guarantee that you will receive a (granted, smaller and smaller) part of all future donations made to the project, which can amount to a lot if the project is successful.
			On the other hand, people arriving late in the project will be less incentivized to contribute to the project: they know their contribution will be rewarded a lot less than the early contributions.
		\item[$p_{min} = 0$]
			Setting $p_{min}$ to a null value means that once your token has reached its minimum power you effectively have zero power in the DAO anymore and no more claim to the donations received.
			Such a strategy keeps interest high for contributors as the total amount of governance token minted can decrease over time.
			\textcolor{red}{Finish this section: Think some more about inflation}
	\end{description}
	
	\subsection{Merge Requests}
	\label{sec:merge_requests}
	
	Merge requests, denoted $mr_i$, are yes/no kind of votes in which participants can propose a git commit as the next official version of the project.
	This turns \gitdao\ smart contracts into keepers of the official version history of a project.
	Merge requests follow the state machine in figure \ref{fig:state_machine}.
	Each merge request has two properties: a \textit{trust level} and a \textit{state}.
	The trust level indicates how much people trust the content of the merge request to be non-adversarial for the project.
	The state property denotes how far in the voting process the merge request is.
	
	\begin{figure*}[ht!]
		\centering%
		\scalebox{.65}{%
			\setlength{\x}{3cm}%
			\setlength{\y}{3cm}%
			\begin{tikzpicture}[
				action/.style = {
					ultra thick,
				},
				action label/.style = {
					font=\ttfamily,
					%						fill = white,
					text width = 2.5cm,
					align = center,
				},
				final/.style = {
%					font = \bfseries,
%					text = white,
					fill = lightgray,
					draw = none,
				},
				node distance = 3cm,
				state/.style = {
					rectangle,
					draw,
					ultra thick,
					text width = 2cm,
					align = center,
					inner sep = 2mm,
				},
				time based/.style = {
					dashed
				},
				trust/.style = {
					draw,
					ultra thick,
					inner sep = 4mm,
				},
				trust label/.style = {
					font=\itshape,
				},
			]
				\node [] (start) at (0\x, 1\y) {Start};
				\node [state] (pending) at (0\x, 0\y) {Pending};
				\node [state] (voting opened) at (0\x, -1\y) {Voting Opened};
				\node [state, final] (canceled) at (-1\x, -1\y) {Canceled};
				\node [state] (voting elapsed) at (0\x, -2\y) {Voting Elapsed};
				\node [state] (finishable) at (0\x, -3\y) {Finishable};
				\node [state, final] (succeeded) at (-0.5\x, -4\y) {Succeeded};
				\node [state, final] (defeated) at (0.5\x, -4\y) {Defeated};
				
				\path [->] (start) edge [action] node [action label, right] {create merge request} (pending);
				\path [->] (pending) edge [action] node [action label, left] {cancel merge request} (canceled);
				\path [->] (pending) edge [action, time based] (voting opened);
				\path [->] (voting opened) edge [action, time based] (voting elapsed);
				\path [->] (voting elapsed) edge [action, time based] (finishable);
				\path [->] (finishable) edge [action] node [action label, left] {finish\\yes $\ge$ no} (succeeded);
				\path [->] (finishable) edge [action] node [action label, right] {finish\\yes $<$ no} (defeated);
				
				\node [trust, fit = (pending) (canceled) (defeated) (succeeded)] (optimistically trusted) {};
				\node [trust label, anchor = south west] at (optimistically trusted.north west) {Optimistically Trusted};
				
				\node (challenged 1) at (1.8\x, 0\y) {};
				\node (challenged 2) at (1.8\x, -1\y) {};
				\node (challenged) at (1.8\x, -1.5\y) [trust label] {Challenged};
				\node (challenged 3) at (1.8\x, -2\y) {};
				\node (challenged 4) at (1.8\x, -3\y) {};
				\node [trust, fit = (challenged) (challenged 1) (challenged 2) (challenged 3) (challenged 4)] (challenged) {};
				
				\path let
				\p1 = (challenged.west),
				\p2 = (challenged 1)
				in [->] (pending) edge [action] (\x1, \y2);
				\path let
				\p1 = (challenged.west),
				\p2 = (challenged 2)
				in [->] (voting opened) edge [action] (\x1, \y2);
				\path let
				\p1 = (challenged.west),
				\p2 = (challenged 3)
				in [->] (voting elapsed) edge [action] node [action label, above=1cm] {challenge} (\x1, \y2);
				\path let
				\p1 = (challenged.west),
				\p2 = (challenged 4)
				in [->] (finishable) edge [action] (\x1, \y2);
				
				\node (challenge elapsed 1) at (3\x, 0\y) {};
				\node (challenge elapsed 2) at (3\x, -1\y) {};
				\node (challenge elapsed) [trust label, align=center] at (3\x, -1.5\y) {Challenge\\Elapsed};
				\node (challenge elapsed 3) at (3\x, -2\y) {};
				\node (challenge elapsed 4) at (3\x, -3\y) {};
				\node [trust, fit = (challenge elapsed) (challenge elapsed 1) (challenge elapsed 2) (challenge elapsed 3) (challenge elapsed 4)] (challenge elapsed) {};
				
				\path let
				\p1 = (challenged.east),
				\p2 = (challenged 1),
				\p3 = (challenge elapsed.west),
				\p4 = (challenge elapsed 1)
				in [->] (\x1, \y2) edge [action, time based] (\x3, \y4);
				\path let
				\p1 = (challenged.east),
				\p2 = (challenged 2),
				\p3 = (challenge elapsed.west),
				\p4 = (challenge elapsed 2)
				in [->] (\x1, \y2) edge [action, time based] (\x3, \y4);
				\path let
				\p1 = (challenged.east),
				\p2 = (challenged 3),
				\p3 = (challenge elapsed.west),
				\p4 = (challenge elapsed 3)
				in [->] (\x1, \y2) edge [action, time based] (\x3, \y4);
				\path let
				\p1 = (challenged.east),
				\p2 = (challenged 4),
				\p3 = (challenge elapsed.west),
				\p4 = (challenge elapsed 4)
				in [->] (\x1, \y2) edge [action, time based] (\x3, \y4);
				
				\node [trust, final, trust label] (blacklisted) at (3\x, -4\y) {Blacklisted};
				
				\path [->] (challenge elapsed.south) edge [action] node [action label, left] {finish challenge\\yes $\ge$ no} (blacklisted);
				
				\node [state] (pending 2) at (4.8\x, 0\y) {Pending};
				\node [state] (voting opened 2) at (4.8\x, -1\y) {Voting Opened};
				\node [state, final] (canceled 2) at (5.8\x, -1\y) {Canceled};
				\node [state] (voting elapsed 2) at (4.8\x, -2\y) {Voting Elapsed};
				\node [state] (finishable 2) at (4.8\x, -3\y) {Finishable};
				\node [state, final] (succeeded 2) at (4.3\x, -4\y) {Succeeded};
				\node [state, final] (defeated 2) at (5.3\x, -4\y) {Defeated};
				
				\path [->] (pending 2) edge [action] node [action label, right] {cancel merge request} (canceled 2);
				\path [->] (pending 2) edge [action, time based] (voting opened 2);
				\path [->] (voting opened 2) edge [action, time based] (voting elapsed 2);
				\path [->] (voting elapsed 2) edge [action, time based] (finishable 2);
				\path [->] (finishable 2) edge [action] node [action label, left] {finish\\yes $\ge$ no} (succeeded 2);
				\path [->] (finishable 2) edge [action] node [action label, right] {finish\\yes $<$ no} (defeated 2);
				
				\node [trust, fit = (pending 2) (canceled 2) (defeated 2) (succeeded 2)] (whitelisted) {};
				\node [trust label, anchor = south west] at (whitelisted.north west) {Whitelisted};
				
				\path let
				\p1 = (challenge elapsed.east),
				\p2 = (challenge elapsed 1)
				in [->] (\x1, \y2) edge [action] (pending 2);
				\path let
				\p1 = (challenge elapsed.east),
				\p2 = (challenge elapsed 2)
				in [->] (\x1, \y2) edge [action] (voting opened 2);
				\path let
				\p1 = (challenge elapsed.east),
				\p2 = (challenge elapsed 3)
				in [->] (\x1, \y2) edge [action] node [action label, above=7mm] {finish challenge\\no $\ge$ yes} (voting elapsed 2);
				\path let
				\p1 = (challenge elapsed.east),
				\p2 = (challenge elapsed 4)
				in [->] (\x1, \y2) edge [action] (finishable 2);
			\end{tikzpicture}
		}
		\caption{%
			\label{fig:state_machine}%
			State machine of \gitdao.
			Dashed transition lines mean that the transition occur automatically after some predefined amount of time.
			Plain lines represent possible transitions, with their name and required conditions if any.
			Italicized state names represent values that the \textit{trust} property can take.
			Normal text state names represent values that the \textit{state} property can take.
			Grayed states are final states.
		}
	\end{figure*}

	Merge requests are defined to be yes/no votes, as a way to incentivize creating binary votes only.
	Binary votes are preferred, because of \href{https://en.wikipedia.org/wiki/Arrow%27s_impossibility_theorem}{Arrow's impossibility theorem} that states that no voting system can guarantee unrestricted domain, non-dictatorship, Pareto efficiency, and independence of irrelevant alternatives if there are more than two options, even when requesting a very general preference ranking over the options from the voters.
	Further, this makes the implementation simpler, thus easier to understand, test and debug which is very important on blockchains.

	\subsubsection{Regular Voting Process}
	\label{sec:merge_requests_regular_volting_process}
	
	Upon creation, a merge request starts with its trust level set to \texttt{Optimistically Trusted}.
	As long as it remains optimistically trusted, the merge request will undergo the following pipeline.
	It starts in state \texttt{Pending}.
	This period of time exists to let users read about the merge request, maybe discuss it off-line, etc.
	As long as the merge request is pending, the proposer can \texttt{cancel} the merge request, i.e. delete it at no other cost than the transaction costs.
	It might make sense for proposers to cancel their merge request, for example if some better alternative is found or if a bug/security issue is detected.
	
	Once the \texttt{Pending} period ends, the merge request will automatically%
	\footnote{%
		On the blockchain, nothing can happen \enquote{automatically}.
		In this case, we mean that whatever write transaction is performed on the merge request, e.g. calling the voting function of the contract, the state will first be updated according to the predefined state timeouts.
		Thus, whatever transaction the user performs, the proposition will always be in the correct state.
		This comes at the cost of additional gas consumption for each transaction.
	} enter the \texttt{Voting Opened} state.
	At this point a merge request cannot be canceled anymore.
	In this state, governance token holders are allowed to vote on the merge request using their tokens.
	A token can be used zero or one time to vote on each merge request.
	Voters can vote in favor if they think the proposed commit should become the new official version, otherwise they can vote against.
	Note that this is not the same as considering the proposed code as adversarial.
	An adversarial is one that tries to actively harm the project or the people using the project.
	A merge request, on the other hand, might be non-adversarial, yet deemed as being not useful for the project or not fulfilling some quality assurances required like code quality, tests, documentation, etc.
	
	After a predefined duration, the proposition will enter the \texttt{Voting Elapsed} state.
	In this period of time, no state-modifying action on the merge request is allowed.
	This gives time to DAO members to review the merge request and potentially challenge it (see below) if they deem that it is adversarial.
	This prevents attacks where someone would create an adversarial merge request, hope that it flies under the radar of the other members of the DAO, wait until the last second to vote in favor of the proposal and finish the proposal immediately.
	
	When the \texttt{Voting Elapsed} duration is elapsed, the merge request will enter the \texttt{Finishable} state in which anyone can call the \texttt{finish} function will will result in the merge request being \texttt{Succeeded} if there is more power assigned to the \enquote{yes} vote, or the merge request being considered \texttt{Defeated} if there is more power assigned to the \enquote{no} vote.
	If the merge request is \texttt{Defeated}, then the proposed commit does not become the new official version.
	If a merge request is \texttt{Succeeded}, then the proposed commit is considered to be the new official version and an event containing this information is emitted.
	Some people will also be rewarded for their contribution to the pull request, as described further in section \ref{sec:merge_requests_rewarding_scheme}.
	
	The semantic of the votes on merge request is that anyone can hold any opinion without any negative consequences.
	I.e. you can vote against the majority, with the majority, in favor or against the merge request, and you have no negative consequences.
	
	The \gitdao\ features no quorum or such requirements.
	Voter fatigue has become a problem for DAOs, especially given that contributing to the DAO is generally not rewarded.
	Quorums require a lot of effort on the DAO's side to make merge request accepted and we wanted to keep the work overhead induced by the \gitdao\ as low as possible so that it remains interesting even for small projects to use it.

	\subsubsection{Challenges}
	\label{sec:merge_requests_challenges}
	
	In any of the non-final merge request states, any governance token holder can trigger a \enquote{challenge}.
	Challenges are a defense mechanism of the DAO against adversarial behaviors.
	While challenges take the form of votes too, their semantic is very different than that of the regular votes: anyone voting against the majority will suffer negative consequences.
	Let's first describe how they work.
	
	Once a challenge is triggered, then the merge request enters the trust level \texttt{Challenged} which pauses any regular voting process and opens another voting procedure: the challenge vote.
	Token holders can participate in the challenge vote using their governance tokens to indicate whether they consider the challenged proposition to be adversarial (in which case they would vote \enquote{yes}) or not (\enquote{no} votes).
	Once the \texttt{Challenged} period times out, the merge request will automatically enter the \texttt{Challenge Elapsed} trust level.
	In this state, no action other than calling the \texttt{finish challenge} function is allowed.
	
	If the power assigned in the challenge vote to \enquote{yes} is greater than that assigned to \enquote{no}, then the merge request ends up with the trust level \texttt{Blacklisted}, all token assigned to the \enquote{no} vote are slashed and the account that initiated the merge request is blacklisted, i.e. all its token are destroyed and the account is banned from ever interacting again with the DAO.
	
	On the other hand if the power assigned to the \enquote{no} vote is greater than that assigned to the \enquote{yes} vote, then the challenge is considered failed, the tokens assigned to \enquote{yes} are slashed, the merge request reaches the \texttt{Whitelisted} trust level and the regular voting procedure resumes exactly where it left off.
	No further challenge can be requested on the given merge request.
	
	We remark that whatever the outcome of the challenge, it is rather brutal: tokens of people voting against the majority get slashed and the merge request initiator might get banned if the challenge succeeds.
	The aim of those challenges is to deter any adversarial behavior against the DAO.
	If someone tries to attack the DAO, for example by proposing a version that contains a worm, then that person might get all its token deleted and its account banned%
	\footnote{
		Banning an account is actually not much of a punition: you can simply create a new one.
		The real incentive is to see your token slashed.
	}%
	.
	The reason why some token are also slashed in case the challenge fails is that challenges stall the progress of a merge request.
	As a way to deter people from trying to launch denial of service attacks against \gitdao, there is also a mechanism to punish token holders start challenges when none is required.
	As the challenges have potentially negative consequences for voters, it is expected that challenges will not be triggered too often.
	They are a safety mechanism of \gitdao.

	\subsection{Rewarding Scheme}
	\label{sec:merge_requests_rewarding_scheme}

	A rewarding scheme, in this context, is a scheme that reward DAO participants with governance token.
	Different rewarding schemes might be required for different types of contributions.
	For now, we consider the rewarding schemes that reward contributing some code/time/energy/coffee to a merge request.

	Coming up with a good rewarding scheme is difficult.
	We wish to find a system that fulfills the following properties at least:

	\begin{description}
		\item[\texttt{Proportional}]
			The reward granted should be proportional to the value of the contribution made.
			If you contribute more, you should be rewarded more.
		\item[\texttt{Non-exploitable}]
			It should be hard to exploit the system to one's advantage, i.e. to be rewarded more than the conceeded contribution.
		\item[\texttt{Minimum-friction}]
			The system should not require too much interactions with the users, so as to prevent voter fatigue.
	\end{description}

	Let's first consider a rewarding scheme that fail at fullfilling the criterias above.
	A notoriously bad strategy is to use the number of lines of code contributed, which incentivizes a coding style that uses as many lines as possible, making the code much harder to read and which does not measure the value of the contribution at all.
	Only the \texttt{minimum-friction} property is achieved.

	This highlights the difficulty in finding some objective and computationally-feasible metric to evaluate the value of a contribution.
	We postulate that the real value of a contribution to a project can only be evaluated by humans.
	Consider for example a person that manage code contributors, brings coffee, come up with the feature ideas, etc. but never contribute code.

	Acknoledging this, we propose a merge request rewarding scheme based on human voting.
	Note that evaluating the value of the contribution of each participant can be divided in two independent tasks:

	\begin{enumerate}
		\item
			Evaluate how valueable the merge request is to the project.
		\item
			Evaluate the proportional importance of the contribution of each account that participated to the merge request.
	\end{enumerate}

	We denote $\hat v_j(mr_i)$ the value of a merge request as estimated by token holder $j$.
	We define the \enquote{official} value of the merge request $v(mr_i)$ to be equal to $\weightedMedian{\hat v_j(mr_i)}$, the weighted median of the votes $\hat v_j(mr_i)$, where the weights are the power of each voter.

	Each voter will also need to evaluate the relative contribution of each contributor in the given merge request.
	We denote $\hat c_k(mr_i, a_j)$ the contribution that voter $k$ believes contributor $j$ participated to the merge request $mr_i$.
	Note that the estimated contributions must satisfy $\sum_j \hat c_k(mr_i, a_j) = 1$.
	The \enquote{official} contribution of contributor $j$ is:

	\[
		c(mr_i, a_j) = \frac{\weightedMedian{\hat c_k(mr_i, a_j)}}{\sum_j \weightedMedian{\hat c_k(mr_i, a_j)}}
	\]

	Each contributor $a_j$ will receive a governance token with an initial power set to $v(mr_i) \cdot c(mr_i, a_j)$.

	We postulate that it is easier for people to estimate the total value of a merge request and the relative individual contributions separately, than it is to evaluate the individual contributions' value directly, which is why we split the task.

	\textcolor{red}{Prove three properties}
	
	\subsection{Rewards}
	
	The \gitdao, being a smart contract, can receive money rather easily.
	We propose that any money received by the smart contract be split three ways:

	\begin{enumerate}
		\item
			The first part is redistributed to other Open Source projects that the DAO wants to help fund.
			We envision that a project will mostly fund its dependencies.
			The project will do this on a voluntary basis.

			Allowing money to flow from one project to another creates a \textit{funding graph}.
			We remark that this prevents having only the most visible projects, like mozilla firefox for example, get all the donations.
			It brings funding to core software infrastructure that normally would not attract donations, e.g. \texttt{libc} which creates a lot of value but atracts very little money.

			From the incentives perspective, it makes sense for a project to donate to its dependencies, because the project has an interest in having its dependencies feature good documentation, rapid bug fixes, improved features, etc.
			If the dependencies are abandoned, the project will have to make effort to replace the dependency or take over its development.
		\item
			The second part is distributed to the token holders of the DAO, proportionally to their \textit{power} at the moment the donation is received.
			It seems fair that the people that contributed to making the software work and thus provided value to users get some value in return.
			Given enough donations, one can imagine people contributing to open source software as their main occupation, altough the uncertainty related to an income being dependent on donation might be a deterrent factor.
		\item
			The third part is hoarded up in the DAO treasury as a reserve that can be used for bug bounties, paying for one-off tasks like branding, or implementing a feature.
	\end{enumerate}

	This situation is summarized in figure~\ref{fig:money_distribution}.
	
	\begin{figure*}[ht!]
		\centering
		\setlength{\x}{4cm}
		\setlength{\y}{2cm}
		\begin{tikzpicture}[
				money_transfer/.style = {
					->,
					thick,
				},
				n/.style = {
					align=center,
					rectangle,
					draw,
					text width=2cm,
				},
			]
			\node[n] (people_donating) at (0\x, -0.5\y) {Blockchain\\Accounts};
			\node[n] (dao_donating) at (0\x, 0.5\y) {Other\\\gitdao s};
			
			\node[n] at (1\x, 0\y) (gitdao) {\gitdao};

			\node[n] at (1\x, -1\y) (token_holders) {Token Holders};
			
			\node[n] at (2\x, -0\y) (dao_receiving) {Other\\\gitdao s};
			
			\path[money_transfer] (dao_donating) edge node[midway, above=4mm] {donations} (gitdao);
			\path[money_transfer] (people_donating) edge (gitdao);
			
			\path[money_transfer, out=120, in=60, loop] (gitdao) edge node[midway, above] {Treasury} (gitdao);
			\path[money_transfer] (gitdao) edge node[midway, align=center] {Direct\\distribution} (token_holders);
			
			\path[money_transfer] (gitdao) edge node[midway, above, align=center] {Further\\funding} (dao_receiving);
		\end{tikzpicture}
		\caption{%
			\label{fig:money_distribution}
			How donations received by the \gitdao\ are used.
			One part is further donated to the dependencies of the current project.
			One part is distributed to the token holders of the DAO as remuneration for their work and the value they provided to users.
			The last part is hoarded up, in order to build the DAO treasury which can be used by the project.
		}
	\end{figure*}
	
	\hrule
	
	Justify why give tokens only to contributors of the DAO and not to users of the DAO.
	
	Historical examples:
	\begin{itemize}
		\item Hostile take over of the library (JS library)
		\item Log4J slow response to a critical security fix
		\item Lack of funding for open source projects, asymmetry between the value they provide and the money they get.
		\item Smaller/less visible projects that never get any money.
	\end{itemize}
	
	
	% \newpage
	% \printbibliography
\end{document}

