% !TeX spellcheck = en_US
% !TeX program = xelatex

\documentclass{gitdao}

\usepackage{nameref}
\usepackage{multicol}

\DeclareDocumentCommand{\securityFeature}{m}{%
	\marginElement{\tikz\node [inner sep = 1mm, fill = pink_500, text = white] {Security Feature};\\#1}%
}

\begin{document}
	\RaggedRight%
	\symmetricalPage%
	\thispagestyle{empty}%
	\null%
	\begingroup%
	
	\begin{tikzpicture}[remember picture, overlay]
		\fill[gray_100] (current page.north west) rectangle (current page.south east);
	\end{tikzpicture}
	\color{white}
	\vspace*{.1\textheight}\\%
	\begin{tikzfadingfrompicture}[name=title]
		\node[
		text=transparent!0
		] {\titleFont\fontsize{42pt}{43.2pt}\selectfont GITDAO};
	\end{tikzfadingfrompicture}
	\begin{tikzpicture}
		\shade[
		path fading=title,
		fit fading=false,
		left color=pink_500,
		right color=violet_500,
		] (-4, -1) rectangle (0, 1);
		\shade[
		path fading=title,
		fit fading=false,
		left color=violet_500,
		right color=blue_500,
		] (0, -1) rectangle (3.8, 1);
	\end{tikzpicture}
	\\[8mm]%
	{\LARGE\light\today}%
	\vfill%
%	\includegraphics[width=1.5cm]{images/nexplorer_logo_400.png}
	\endgroup%
	
	\newpage%
	\asymmetricalPage%
	
	\marginElement{
		\begingroup
			\Huge
			\fontspec{roboto_regular.ttf}
			\textcolor{pink_500}{GitDAO}
			\\[2mm]
		\endgroup
		\large%
		\fontspec{roboto_regular_italic.ttf}
		\color{gray_100}%
		\selectfont%
		We bring\\
		decentralized\\
		governance\\
		to Open Source
	}

%	Introduction
%	Purpose
%	Examples
%	The Cathedral and the Bazaar
%	
%	functionalities
%		Integrate with Radicle
%		Mon-shareables tokens
%		Time limited tokens
%		Voting workflow
		
	\section{Introduction}
	
	This project aims to bring more decentralization, security and robustness to the Open Source ecosystem.
	In order to achieve this goal, we created a DAO template that each Open Source projects can deploy for governance, but also funding and many more features.
	
	\begin{fullwidth}
		\newlength{\threecolumnwidth}%
		\setlength{\threecolumnwidth}{\linewidth}%
		\addtolength{\threecolumnwidth}{-2\columnsep}%
		\setlength{\threecolumnwidth}{\threecolumnwidth/3}%
		\vspace*{-4mm}%
		\parbox{\threecolumnwidth}{%
			\ycard{
				\ycimage[Open Source]{images/open_source_2.png}
			}%
		}\hspace*{\columnsep}%
		\parbox{\threecolumnwidth}{%
			\ycard{%
				\ycimage[Governance]{images/governance.jpg}
			}%
		}\hspace*{\columnsep}%
		\parbox{\threecolumnwidth}{%
			\ycard{%
				\ycimage[Distribution]{images/donations.jpg}%	
			}%
		}%
	\end{fullwidth}

	But let us maybe illustrate first why such improvements are required:
	
	\marginElement{%
		\rlap{
			\fontsize{2cm}{1.2cm}\selectfont%
			\textcolor{blue_500!25!white}{\bfseries«}%
		}%
		\vspace*{-5mm}%
		If you want to go fast, go alone.
		If you want to go far, go together.\\
		\null\hfill--- African Proverb
	}
	\begin{itemize}
		\item Most Open Source projects rely on a Benevolent Dictator For Life (BDFL), think of Linus Torvald for example.
		We postulate that this model is neither democratic nor resilient or robust.
		\item Many smaller project lack an incentivization layer, i.e. funding, to complete less fun tasks like testing and writing documentation.
		This lowers code quality and represents a lost opportunity.
		We postulate that Open Source users are sometimes willing to pay to improve quality assurances, so this is really a coordination failure.
	\end{itemize}

	History is rich of coordination failure histories.
	Let us nevertheless recall one of particular interest...
	
	This story begins on the 10\textsuperscript{th} of December 2021; the US government releases a warning concerning a critical Remote Code Execution (RCE) vulnerability discovered in the Open Source Java logging library \texttt{Log4j}.
	The vulnerability is evaluated as \enquote{Critical} by the US Agency responsible for Cyber Security, CISA.
	Unfortunately \texttt{Log4j} is very widely used across the internet, and soon enough screenshots of important websites like Amazon, Apple, Google or Tencent being exploited circulate online.
	Six days after the original post by the CISA, Jen Easterly, head of the CISA, declares on CNBC this vulnerability to be \enquote{the most serious vulnerability [she] ha[s] seen in [her] decade-long career}.
	
	\marginElement{%
		\includegraphics[width=\linewidth]{images/sponsors.png}
	}
	As it happened, the \texttt{Log4j} library was being maintained by one developer: Ralph Goers.
	It had three sponsors.
	
	\section{Radicle}
	
	\marginElement{See \url{radicle.xyz}}
	\marginElement{\includegraphics[width=\linewidth]{images/radicle.png}}
	Radicle is a peer-to-peer alternative to centralized code hosting solutions like GitHub.
	It uses features already built into git to provide a decentralized way of sharing code.
	In this new paradigm, everyone keeps a copy of the code and some more social metadata are stored along in the repository, for example merge request data.
	
	This new way of sharing code, even though it provide many advantages, like the lack of reliance on a single centralized entity, also induces new challenges.
	For example, how does one track the \enquote{official} version of an Open Source project?
	
	Radicle provides functionalities to deploy Gnosis Safes that track the hash of the commit of the version deemed as \enquote{official} by the members of the safe.
	The aim of this project is to build a alternative specifically targeted at this task.
	
	\section{GitDAO}
	
	GitDAO is a DAO template for decentralized Open Source projects that provides workflows for managing versions and many other aspects.
	The DAO features a voting process, so-called proposals, in which people can propose a commit to consider as the new \enquote{official} version of the project.
	
	\subsection{Proposals}
	
	When creating a proposal, Open Source developers register a commit hash and they have to lock some tokens into the DAO, e.g. 10 DAI.
	Those tokens will be given back to them once the proposal is either successful or rejected.
	\securityFeature{DoS attacks resistance}This prevents a single actor from opening numerous proposals to perform a DoS attacks against the DAO.
	The proposal will remain pending for a fixed amount during which members of the DAO can review the code.
	
	Then the voting period opens.
	DAO members can vote in favor of accepting the commit as the new official version of the DAO or against it.
	If the vote passes, the proposer will receive a governance token in return for its contribution to this version of the project.
	
	It might be the case that the project is attacked by an adversarial actor.
	For example, someone might wish to include a virus in the code of a widely used Javascript library in order to leak wallet passwords\marginNote{See \href{https://www.zdnet.com/article/hacker-backdoors-popular-javascript-library-to-steal-bitcoin-funds/}{the \texttt{event-stream} hack}.}.
	In such case a member of the DAO might trigger a challenge on the proposal.
	To challenge a proposal, one will have to stack some governance token.
	Then the community can vote on the challenge.
	\securityFeature{Adversarial proposal defense mechanism}If the challenge passes, then the proposal's proposer will be banned from the DAO and the token it locked for its proposal are slashed.
	
	\begin{fullwidth}
		\scalebox{.65}{
			\newlength{\x}
			\setlength{\x}{3cm}
			\newlength{\y}
			\setlength{\y}{3cm}
			\begin{tikzpicture}[
					action/.style = {
						ultra thick,
					},
					action label/.style = {
						font=\fontspec{Roboto Mono},
%						fill = white,
						text width = 2.5cm,
						align = center,
					},
					final/.style = {
						double
					},
					node distance = 3cm,
					state/.style = {
						rectangle,
						draw = blue_500,
						ultra thick,
						text width = 2cm,
						align = center,
						inner sep = 2mm,
						rounded corners=1mm,
					},
					time based/.style = {
						dashed
					},
					trust/.style = {
						draw = pink_500,
						ultra thick,
						inner sep = 4mm,
					},
					trust label/.style = {
						font = \bfseries,
						text = white,
						fill = pink_500,
					},
				]
				\node [] (start) at (0\x, 1\y) {Start};
				\node [state] (pending) at (0\x, 0\y) {Pending};
				\node [state] (voting opened) at (0\x, -1\y) {Voting Opened};
				\node [state, final] (canceled) at (-1\x, -1\y) {Canceled};
				\node [state] (voting elapsed) at (0\x, -2\y) {Voting Elapsed};
				\node [state, final] (succeeded) at (-0.5\x, -3\y) {Succeeded};
				\node [state, final] (defeated) at (0.5\x, -3\y) {Defeated};
				
				\path [->] (start) edge [action] node [action label, right] {create proposal} (pending);
				\path [->] (pending) edge [action] node [action label, left] {cancel proposal} (canceled);
				\path [->] (pending) edge [action, time based] (voting opened);
				\path [->] (voting opened) edge [action, time based] (voting elapsed);
				\path [->] (voting elapsed) edge [action] node [action label, left] {finish\\yes $\ge$ no} (succeeded);
				\path [->] (voting elapsed) edge [action] node [action label, right] {finish\\yes $<$ no} (defeated);
				
				\node [trust, final, fit = (pending) (canceled) (defeated) (succeeded)] (optimistically trusted) {};
				\node [trust label, anchor = south west] at (optimistically trusted.north west) {Optimistically Trusted};
				
				\node (challenged 1) at (1.8\x, 0\y) {};
				\node (challenged 2) at (1.8\x, -1\y) {Challenged};
				\node (challenged 3) at (1.8\x, -2\y) {};
				\node [trust, fit = (challenged 1) (challenged 2) (challenged 3)] (challenged) {};
				
				\path let
					\p1 = (challenged.west),
					\p2 = (challenged 1)
				in [->] (pending) edge [action] (\x1, \y2);
				\path let
					\p1 = (challenged.west),
					\p2 = (challenged 2)
				in [->] (voting opened) edge [action] node [action label, above] {challenge} (\x1, \y2);
				\path let
					\p1 = (challenged.west),
					\p2 = (challenged 3)
				in [->] (voting elapsed) edge [action] (\x1, \y2);
				
				\node (challenge elapsed 1) at (3\x, 0\y) {};
				\node (challenge elapsed 2) [align=center] at (3\x, -1\y) {Challenge\\Elapsed};
				\node (challenge elapsed 3) at (3\x, -2\y) {};
				\node [trust, fit = (challenge elapsed 1) (challenge elapsed 2) (challenge elapsed 3)] (challenge elapsed) {};
				
				\path let
					\p1 = (challenged.east),
					\p2 = (challenged 1),
					\p3 = (challenge elapsed.west),
					\p4 = (challenge elapsed 1)
				in [->] (\x1, \y2) edge [action, time based] (\x3, \y4);
				\path let
					\p1 = (challenged.east),
					\p2 = (challenged 2),
					\p3 = (challenge elapsed.west),
					\p4 = (challenge elapsed 2)
				in [->] (\x1, \y2) edge [action, time based] (\x3, \y4);
				\path let
					\p1 = (challenged.east),
					\p2 = (challenged 3),
					\p3 = (challenge elapsed.west),
					\p4 = (challenge elapsed 3)
				in [->] (\x1, \y2) edge [action, time based] (\x3, \y4);
				
				\node [trust, final] (blacklisted) at (3\x, -3\y) {Blacklisted};
				
				\path [->] (challenge elapsed.south) edge [action] node [action label, left] {finish challenge\\yes $\ge$ no} (blacklisted);
				
				\node [state] (pending 2) at (4.8\x, 0\y) {Pending};
				\node [state] (voting opened 2) at (4.8\x, -1\y) {Voting Opened};
				\node [state, final] (canceled 2) at (5.8\x, -1\y) {Canceled};
				\node [state] (voting elapsed 2) at (4.8\x, -2\y) {Voting Elapsed};
				\node [state, final] (succeeded 2) at (4.3\x, -3\y) {Succeeded};
				\node [state, final] (defeated 2) at (5.3\x, -3\y) {Defeated};
				
				\path [->] (pending 2) edge [action] node [action label, right] {cancel proposal} (canceled 2);
				\path [->] (pending 2) edge [action, time based] (voting opened 2);
				\path [->] (voting opened 2) edge [action, time based] (voting elapsed 2);
				\path [->] (voting elapsed 2) edge [action] node [action label, left] {finish\\yes $\ge$ no} (succeeded 2);
				\path [->] (voting elapsed 2) edge [action] node [action label, right] {finish\\yes $<$ no} (defeated 2);
				
				\node [trust, final, fit = (pending 2) (canceled 2) (defeated 2) (succeeded 2)] (whitelisted) {};
				\node [trust label, anchor = south west] at (whitelisted.north west) {Whitelisted};
				
				\path let
					\p1 = (challenge elapsed.east),
					\p2 = (challenge elapsed 1)
				in [->] (\x1, \y2) edge [action] (pending 2);
				\path let
					\p1 = (challenge elapsed.east),
					\p2 = (challenge elapsed 2)
				in [->] (\x1, \y2) edge [action] node [action label, above] {finish challenge\\no $\ge$ yes} (voting opened 2);
				\path let
					\p1 = (challenge elapsed.east),
					\p2 = (challenge elapsed 3)
				in [->] (\x1, \y2) edge [action] (voting elapsed 2);
			\end{tikzpicture}
		}\\%
		GitDAO State Machine
	\end{fullwidth}

	\marginElement{\includegraphics[width=\linewidth]{images/coin.png}}
	\subsection{Governance Tokens}
	
	The governance tokens awarded upon a successful proposal have a few properties worth mentioning:
	
	\begin{itemize}
		\item Non-transferable
		\item Decreasing value over time
		\item ERC721
	\end{itemize}

	First, the governance tokens are \textbf{not transferable}.
	The idea of this project is not to create another get-rich-fast stratagem.
	Instead, it aims at improving the situation of Open Source.
	We remark here that Open Source strives\marginNote{Think of projects like Linux, Python, or Wikipedia.} without the need for monetary incentives, so great care must be taken to improve where things can be improved and leave what cannot be improved unchanged.
	Making the governance token transferable, means that it becomes possible to create exchanges and markets for these tokens, people might want to participate in the project because they bet that the token will take value over time.
	We prefer that developers contribute because of some inner drive rather, like they do today.
	
	What's more, the token are ERC721 and they have a property called \enquote{power}.
	This power represents, quite literally, the power that you have in the DAO.
	This power decreases over time, i.e. \textbf{the token loose some value over time}.
	The rational behind this decision is that we want people to have a say in the DAO as long as they contribute.
	If you stop contributing for years to a project, when you come back, the project most probably has evolved, you do not know the code anymore, nor how things are done.
	
	\newpage
	\marginElement{\includegraphics[width=\linewidth]{images/rocket.png}}
	\section{Not-Yet-Implemented Features}
	
	In the future, we will add more functionalities to this core, including:
	
	\begin{itemize}
		\item Integrate with the drips protocol from Radicle.
		The drips protocol aims to simplify recurring payments and it allows giving further some share of the money you receive to other accounts on the blockchain.
		This makes total sense for Open Source project that most often rely on other Open Source projects.
		\item Automatic distribution of the donations made to the project to the developers proportionally to the power they have in the DAO at the moment of the donation.
		\item Predefined rewards for set tasks like writing documentation or tests.
		\item Bug bounty programs.
	\end{itemize}

	\section{Team}
	
	\textbf{Yves}
	\marginElement{%
		\null\hfill\begin{tikzpicture}
			\clip (0, 0) circle (1.5cm);
			\node {\includegraphics[width=5cm]{images/yves.jpg}};
		\end{tikzpicture}
	}
	
	MSc Computer Science ETHZ\\
	3+ years working in startups\\
	Blockchain Addict
	
	\vspace*{1cm}

	\textbf{Andy}
	\marginElement{
		\null\hfill\begin{tikzpicture}
			\clip (0, 1) circle (1.5cm);
			\node {\includegraphics[width=4cm]{images/andy.png}};
		\end{tikzpicture}
	}
	
	Msc Biomedical Sciences\\
	Fascinated by engineering\\
	Asking too many \enquote{why's}
	
\end{document} 
