% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Basic configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{gitdao}

\DeclareOption*{
	\PassOptionsToClass{\CurrentOption}{article}
}

\ProcessOptions\relax

\LoadClass[letterpaper, fleqn, oneside, 11pt]{article}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Various packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[usenames,dvipsnames,svgnames,table]{xcolor}

\RequirePackage{amsmath, amsfonts, amssymb}
\RequirePackage{booktabs}
\RequirePackage{calc}
\RequirePackage{caption}
\RequirePackage{csquotes}
\RequirePackage{enumitem}
\RequirePackage{etoolbox}
\RequirePackage{fancyhdr}
\RequirePackage{floatrow}
\RequirePackage{fullwidth}
\RequirePackage{graphicx}
\RequirePackage{ifthen}
\RequirePackage{marginfix}
\RequirePackage{marginnote}
\RequirePackage{microtype}
\RequirePackage{minted}
\usepackage[all]{nowidow}
\RequirePackage{ragged2e}
\RequirePackage{setspace}
\RequirePackage{tikz}
\RequirePackage{tikzpagenodes}
\RequirePackage[compact]{titlesec}
\RequirePackage{titletoc}
\RequirePackage{xparse}
\RequirePackage{zref-abspos}

\usetikzlibrary{calc, fadings, shapes, fit, positioning, matrix, positioning}

\RequirePackage{infoBulle}
\RequirePackage{marginInfoBulle}
\RequirePackage{yCards}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Language
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{polyglossia}
\setdefaultlanguage{english}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{geometry}
\geometry{
	a4paper,
	includeheadfoot,
	left=2.2cm,
	right=2.2cm,
	top=2.2cm,
	bottom=2.2cm,
	footskip=0cm,
	footnotesep=0mm,
	headheight=0pt,
	headsep=0pt,
	includemp,
	reversemp,
	marginparwidth=4.5cm,
	columnsep=8mm,
	marginparsep=1cm,
}

\setlength{\parskip}{\baselineskip}
\setlength{\parindent}{0ex}

\onehalfspacing

\newcommand{\symmetricalPage}{
	\fancyhfoffset[L]{0mm}
	\newgeometry{
		includeheadfoot,
		left=2.7cm,
		right=2.7cm,
		top=2.7cm,
		bottom=2.7cm,
		marginparwidth=0cm,
		marginparsep=0mm
	}
}
\newcommand{\asymmetricalPage}{
	\restoregeometry
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Font
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{fontspec}
\setmainfont{roboto_slab_regular.ttf}[
	BoldFont=roboto_slab_bold.ttf
]

\setmonofont{roboto_mono_regular.ttf}

\setfontfamily{\light}{roboto_slab_light.ttf}

\setfontfamily{\FA}{font_awesome_5_free_400.otf}
\setfontfamily{\FASolid}{font_awesome_5_free_900.otf}

\newfontfamily{\titleFont}{anurati_regular.ttf}[
LetterSpace=32,
]

\setfontfamily{\titlingFont}{roboto_regular_italic.ttf}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Colors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\providecolor{gray_50}{HTML}{0C0D0E}
\providecolor{gray_100}{HTML}{171A1C}
\providecolor{gray_500}{HTML}{75838A}
\providecolor{gray_900}{HTML}{E3E6E8}

\colorlet{textColor}{gray_100}

\providecolor{pink_500}{HTML}{F368E0}
\providecolor{violet_500}{HTML}{8870FF}
\providecolor{blue_500}{HTML}{00A8FF}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Links
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{hyperref}
\hypersetup{
	pdfborder={1 1 0},
	pdfcreator=LaTeX,
	colorlinks=true,
	linkcolor=blue_500,
	linktoc=all,
	urlcolor=blue_500,
	citecolor=blue_500,
	filecolor=blue_500
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Headers/Footers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\fancypagestyle{trafluid}{
	\renewcommand{\headrulewidth}{0pt}
	\fancyhf{}
%	\cfoot{\hypersetup{linkcolor=textColor}\thepage}
}
\pagestyle{trafluid}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Table of content
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\tableofcontents{\begingroup\hypersetup{linkcolor=textColor}\@starttoc{toc}\endgroup}

\titlecontents{section}%
[0em]%
{}%
{}%
{}%
{\hfill\contentspage}%

\titlecontents{subsection}%
[1.5em]%
{}%
{}%
{}%
{\hfill\contentspage}%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Titling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titleformat{\section}{}{}{0mm}{\color{violet_500}\scshape\Large\hspace*{-1pt}}
\titlespacing{\section}{0pt}{*0}{*0}

\titleformat{\subsection}{}{}{0mm}{\color{blue_500}\scshape\large}
\titlespacing{\subsection}{0pt}{*0}{*0}

\titleformat{\subsubsection}{}{}{0mm}{\scshape}
\titlespacing{\subsubsection}{0pt}{*0}{*0}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Full-Width Environment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newlength{\fullparwidth}
\setlength{\fullparwidth}{\textwidth}
\addtolength{\fullparwidth}{\marginparsep}
\addtolength{\fullparwidth}{\marginparwidth}
\fullwidthsetup{
	leftmargin = -\marginparsep - \marginparwidth,
	width = \fullparwidth
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Captions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareCaptionFont{trafluidCaptionFont}{\scshape\color{textColor}}
\captionsetup{labelfont=trafluidCaptionFont, font={color=textColor}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Full-Width Figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareFloatVCode{vskip}{{\vskip8mm}}

\floatsetup[widefigure]{%
	margins = hangright,
	capposition = bottom,
	precode = vskip,
	postcode = vskip
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Enumitem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlist[enumerate]{font=\scshape}
\setlist[description]{font=\scshape}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Minted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usemintedstyle{pastie}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Hooks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\AtBeginDocument{\color{textColor}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Backgrounding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{background}[page]
\renewcommand{\thebackground}{\arabic{page}-\arabic{background}}

\newcommand{\backgroundAnchor}[1]{\leavevmode\zsavepos{background-#1}}

% outer_space inner_space
\DeclareDocumentCommand{\startBackground}{O{0mm} O{1em}}{
	\vspace*{#1}%
	\backgroundAnchor{begin-\thebackground}%
	\vspace*{#2}\\%
}

\newlength{\depthLength}
\settodepth{\depthLength}{p}

\newlength{\backgroundTempLength}

% outer_space inner_space
\DeclareDocumentCommand{\stopBackground}{O{0mm} O{0mm}}{
	\setlength{\backgroundTempLength}{#2+2\depthLength}
	\vspace*{\backgroundTempLength}%
	\backgroundAnchor{end-\thebackground}%
	\setlength{\backgroundTempLength}{#1+1em}
	\vspace*{\backgroundTempLength}%
}

\tikzset{background/.style = {fill=gray_900}}
\DeclareDocumentCommand{\drawBackground}{t^ t_}{%
	\leavevmode%
	\stepcounter{background}%
	\zsavepos{background-draw-\thebackground}%
	\begin{tikzpicture}[remember picture, overlay, inner sep=0mm]
		\def\yTop{\zposy{background-begin-\thebackground}sp - \zposy{background-draw-\thebackground}sp}
		\IfBooleanT{#1}{%
			\def\yTop{\y1}
		}
		\def\yBottom{\zposy{background-end-\thebackground}sp   - \zposy{background-draw-\thebackground}sp}
		\IfBooleanT{#2}{%
			\def\yBottom{\y2}
		}
		\fill[background] let
		\p1=(current page.north west),
		\p2=(current page.south east) in
		(\x1, \yTop) rectangle
		(\x2, \yBottom);
	\end{tikzpicture}%
	\ignorespaces
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%																			   %
%																			   %
%								yMarginDesign								   %
%																			   %
%																			   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Global Options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pgfkeys{
	/yMarginDesign/.cd,
	alignment/.code = \RaggedRight,
	emph color/.initial = pink_500,
	text color/.initial = gray_50,
	size/.code = \footnotesize,
	titleFormat/.code = \normalsize\color{\pgfkeysvalueof{/yMarginDesign/emph color}},
	marginparskip/.initial = 8mm,
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Margin Paragraphs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Typeset a paragraph in the margin
\DeclareDocumentCommand{\marginElement}{m}{%
	\marginpar{{%
		\pgfkeys{/yMarginDesign/alignment}%
		\pgfkeys{/yMarginDesign/size}%
		\color{\pgfkeysvalueof{/yMarginDesign/text color}}
		#1\par\vspace{\pgfkeysvalueof{/yMarginDesign/marginparskip}}%
	}}\unskip%
}%

% The mark for the marginNote
\DeclareDocumentCommand{\marginMark}{m}{{\color{\pgfkeysvalueof{/yMarginDesign/emph color}}#1.~}}

\DeclareDocumentCommand{\printMarginNoteMark}{}{%
	{\hypersetup{linkcolor=\pgfkeysvalueof{/yMarginDesign/emph color}}\footnotemark}%
}

% Typeset a paragraph in the margin with a number (as a footnote) 
\DeclareDocumentCommand{\marginNote}{m}{%
	\printMarginNoteMark%
	\ignorespaces%
	\marginElement{%
		\marginMark{\thefootnote}%
		\ignorespaces%
		#1%
	}%
}%

% Typeset a paragraph in the margin exactly at the exact position where the command is called
\DeclareDocumentCommand{\forcedMarginElement}{m}{%
	\marginnote{%
		\pgfkeys{/yMarginDesign/alignment}\footnotesize%
		#1%
	}%
}

% Typeset a paragraph in the margin exactly at the exact position where the command is called with a number (as a footnote)
\DeclareDocumentCommand{\forcedMarginNote}{m}{%
	\printMarginNoteMark
	\ignorespaces%
	\forcedMarginElement{%
		\sideMark[#1]{\thefootnote}%
		\ignorespaces%
		#1%
	}%
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Margin Design Elements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareDocumentCommand{\marginTitle}{m}{{\pgfkeys{/yMarginDesign/titleFormat}#1}\\}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		yCards
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ycardsset{
	line width = 0pt,
	rounded corners = 3mm,
	image/font = \normalsize,
	image/text vertical align=bottom,
	shadow/.style = {
		blur shadow = {%
			shadow xshift=0mm,%
			shadow yshift=-.5mm,%
			shadow opacity=25,%
			shadow blur radius=3mm,%
			shadow blur steps=30,%
		},
	},
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		marginInfoBulle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\definecolor{questionColor}{HTML}{DE6DF7}%{E596F7}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%																			   %
%																			   %
%								yPrintImageToArea							   %
%																			   %
%																			   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Command for image printing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Command that print an image that fills an minimum area
%-------------------------------------------------------------------------------
% horizontal length, vertical length, image path
\newlength{\tempTitlepageImageHeight}%
\DeclareDocumentCommand{\printImageToMinArea}{m m m}{%
	\settototalheight{\tempTitlepageImageHeight}{\includegraphics[width=#1]{#3}}%
	\ifthenelse{\lengthtest{\tempTitlepageImageHeight<#2}}{%
		\includegraphics[height=#2]{#3}%
	}{%
		\includegraphics[width=#1]{#3}%
	}%
}%

