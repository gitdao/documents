% !TeX spellcheck = en_US
% !TeX program = pdflatex
\documentclass{article}

\usepackage{arxiv}
\usepackage{csquotes}
\usepackage{float}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{microtype}
\usepackage{multicol}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric}

\usepackage[
	backend=biber,
	citestyle=numeric,
	style=numeric
]{biblatex}
\bibliography{git_dao}

\author{%
	Yves \textsc{Zumbach}\\
	\texttt{yzumbach@student.ethz.ch}\\
	D-INFK, ETH Zürich
}
\title{Putting Git on the Blockchain, a DAO Project}
\subtitle{The Missing Layers of Open Source}

\begin{document}
	
	\maketitle
	
	\begin{abstract}
		\texttt{Git} is a technological solution for code, \enquote{Open Source} is a legal framework for code.
		Still missing are governance and incentivization layers which we propose to implement on the blockchain as a wrapper around \texttt{git}.
	\end{abstract}

	\vspace{15mm}
	\begin{multicols*}{2}
	
	\section{Introduction --- A \texttt{Log4j} History}
			
	On December 10th 2021, the US government releases a warning concerning a critical RCE (Remote Code Execution) vulnerability discovered in the Open Source Java logging library \texttt{Log4j} \cite{ApacheReleasesLog4j}.
	The vulnerability is evaluated as \enquote{Critical} by the US Agency responsible for Cyber Security, CISA.
	Unfortunately \texttt{Log4j} is used literally throughout the internet, and soon enough screenshots of highly popular websites like Amazon, Apple, Google or Tencent being exploited circulate online \cite{yfrytchsgdYfryTchsGDLog4jAttackSurface2022}.
	Six days after the original post by the CISA, Jen Easterly, head of the CISA, declares on CNBC this vulnerability to be \enquote{the most serious vulnerability [she] ha[s] seen in [her] decade-long career} \cite{CISADirectorSays2021}.

	Unfortunately, \texttt{Log4j} is, by far, not the only Open Source library that received attention recently because of security flaws.
	Many have been quick to say that Open Source is inherently flawed \cite{watersLog4jHackRaises2021}.
	Is the Open Source model still viable?
	
	\section{Context}
	
	\subsection{The Open Source Economical Model, or Lack Thereof}
	
	Today, the importance of Open Source software is rather undisputed: it is used everywhere and at every level of our technology stacks.
	One can think, for example, of the Linux kernel running in the pocket of billions of humans (Android), Python or the Apache HTTP server that powers the web.
	Bottom line: Open Source is not going away.
	
	Most of these projects are maintained by passionate developers without monetary compensations.
	Of course, one cannot expect that they provide all the qualities that enterprise-grade software requires.
	NB: Currently, the \texttt{Log4j} library is being maintained by one developer: Ralph Goers, and it has three sponsors.
	See also figure \ref{fig:log4j_funding}.

	This highlights a recurring problem: Open Source softwares provides billions in value annually, yet most of them receive no money in return and rely on the dedication of a few developers to spend their free time working on them.
	
	In essence, Open Source needs an incentivization scheme that is resistant to a change of jobs, having a child or a burnout.
	
	\begin{figure}[H]
		\includegraphics[width=\linewidth]{images/sponsors.png}
		\caption{The current funding situation of the Open Source Java library \texttt{Log4j} \cite{SponsorsPngPNG}.}
		\label{fig:log4j_funding}
	\end{figure}
	
	\subsection{The Open Source Governance Model, or Lack Thereof}
	
	Because Open Source allows forks, the question of how open source projects should be governed has not received much attention.
	Indeed, if you do not agree with the leadership or the direction taken by the project, you can fork the project and continue on your own.
	However, such events are generally quite damageable to Open Source projects, because they divide the community underlying them.
	Less developers spending time on a piece of software means less bug fixes, less support, less features: an Open Source project is only as good as its community.
	
	Currently, the default governance model can be compared to a dictatorship, i.e. the project initiator keeps complete control over the project.
	The model is actually so well spread that it has a name: the Benevolent Dictator for Life, or BDFL \cite{BenevolentDictatorLife2021}.
	We believe that the BDFL model is widespread because it is the default proposed by online hosting solutions like GitHub or GitLab.
	We also postulate that providing governance tools for Open Source projects more akin to democracies would align better with the Open Source philosophy and provide greater value to society as it might prevent forks, make the community feel more involved and decentralized power.
	As they say: if you want to go fast, go alone; if you want to go far, go together\footnote{%
		Actually, some implementations would make it perfectly possible to go fast at first and to go far then, by decentralizing the power over the project gradually over time.%
	}.
	
	\subsection{Decentralized Autonomous Organizations}
	
	The blockchain movement propose some radical new ideas.
	One of them is the Decentralized Autonomous Organization, or DAO, which designates a smart contract on a blockchain that encodes a governance process, generally based on votes.
	For example, these tools can be used to transfer control to the community over some tuning parameters, e.g. the fee rate of a DEX or the collateralization level of some stablecoin.
	Further, because DAO are smart contracts on a blockchain, they can manage cryptocurrencies and are well suited for funding purposes, like the MolochDAO \cite{MolochDAO} that funds public good projects.
	
	\section{Problem}
	
	In a nutshell, \enquote{Open Source} is a legal framework for code.
	\texttt{Git} is a technological solution for creating and collaborating easily on Open Source projects.
	Open Source is still missing:
	
	\begin{description}
		\item[An incentivization or economical layer] to reward/incentivize efficiently people that contribute through features or by maintaining some quality of interest like security, test coverage, documentation or support.
		\item[A governance layer] to steer in more democratic ways the direction taken by Open Source projects.
	\end{description}

	\section{Proposal}
	
	We propose to implement a DAO specifically targeted at managing open source projects.
	The governance model featured in the DAO will need to match the specific requirements of Open Source projects.
	For example, it might feature a specific type of proposal for bug fixes that is automatically accepted, unless there is some opposition raised, so as to prevent voter fatigue.
	
	Being hosted on a blockchain, it will have a built-in economical layer that will make it easy to donate to the project and to remunerate developers that contribute to the project, e.g. bounty could be awarded upon the acceptance of a pull request.
	The DAO may also distribute ownership of the project to those that contribute by minting and giving governance tokens, which would incrementally decentralize the control over the project.
	
	We further propose to implement a decentralized connection between the DAO and the code, so that the DAO can autonomously merge pull requests when they are accepted through the governance process.
	This could be implemented either through an oracle connecting the DAO and an external git hosting solution, or by reimplementing git on the blockchain directly.
	The chosen implementation will depend on the constraints of the underlying blockchain like smart contract size limit, computational fees, and storage fees.
	This would decentralize in a fully trustless and permisionless way the control over open source projects.
	Similar ideas are already implemented on the Internet Computer with its governance system called the Network Nervous System \cite{dfinityNetworkNervousSystem2021}.
	
	This project combines computer science, economics, and game theory.
	It includes:
	
	\begin{itemize}
		\item Exploring democratic governance processes of Open Source projects.
		\item Exploring existing DAO solutions.
		\item Defining properties of interest for the Open Source-focused DAO.
		\item Implementing the DAO.
		\item Implementing a connection between the DAO and the code to make the system autonomous and trustless.
	\end{itemize}
	
	Main difficulties:
	
	\begin{itemize}
		\item Design a DAO that is desirable for open source project creators.
		\item Design a DAO that rewards contributors of the open source in a fair way.
		\item Design a DAO that proposes an efficient (fast development, no voter fatigue), yet attack-resistant (e.g. through quorum voting), governance model for open source projects.
		\item Design a DAO that is trivial to deploy so that there is very little to no entry barrier to using it.
		\item \textit{Design a way for the DAO to manage the code autonomously and trustlessly.}
	\end{itemize}

	We remark here that although the last entry regarding building the connection between the DAO and the code represents only one fifth of the text, it is clearly the most complex part of this proposal, that it will require researches and a careful implementation so as to boast desired properties like trustlessness and being permissionless.

	\subsection{Further Ideas}
	
	\paragraph{Funding Graph}
	
	The above idea provides tools to distribute money once you have it, but it does not help getting the money.
	A common problem in the Open Source ecosystem is that low level projects that receive less attention from the public also get less donations.
	It is much more common to give money to Wikipedia or Firefox than it is to give to an obscure Javascript library or to the \texttt{libc}, although they respectively power half of the internet and are the key that makes every computer on the Earth compute.
	This does not seem fair and it could be interesting to explore ways of building a dependency graph between Open Source projects, so that parts of the donations made to any project are distributed to the Open Source dependencies recursively.
	
	\paragraph{Liquid Democracy}
	
	If the community behind an Open Source project becomes large enough, empowering it with liquid democracy governance would be interesting \cite{LiquidDemocracy2021}.
	
	\section{Technicalities}
	
	The project will be conducted at SWITCH \cite{ContactUsSWITCH} under the supervision of Robert Ott.
	The target blockchain is the Swiss, EVM-compatible Dragonfly blockchain \cite{PostFeedLinkedIn,MantisDragonflyTestnet2021}, which is based on proof-of-authority and which was recently opened to the public.
\end{multicols*}
	
\newpage
\section{ToDo}

\newcommand\score[1]{%
	\pgfmathsetmacro\pgfxa{#1 + 1}%
	\tikzstyle{scorestars}=[star, star points=5, star point ratio=2.25, draw, inner sep=1.3pt, anchor=outer point 3]%
	\begin{tikzpicture}[baseline]
		\foreach \i in {1, ..., 5} {
			\pgfmathparse{\i<=#1 ? "yellow" : "gray"}
			\edef\starcolor{\pgfmathresult}
			\draw (\i*1.75ex, 0) node[name=star\i, scorestars, fill=\starcolor]  {};
		}
	\end{tikzpicture}%
}

In the following, each sub-item's time estimation is relative to its super-item, i.e. a sub-item with time estimation 5 of a super-item with time estimation 1, will likely take all the time of the super-item.

\begin{itemize}
	\item Create a DAO for Open Source\hfill\score{2}
	\begin{itemize}
		\item Setup website, online documentation, chat, etc.\hfill\score{1}
		\item Analyze needs of OS projects, list features to build into the DAO\hfill\score{1}
		\item Implement the DAO\hfill\score{3}
	\end{itemize}
	\item Create a decentralized, permissionless link between the code and the DAO\hfill\score{3}
	\begin{itemize}
		\item A list of task is not yet available: some additional information and researches are required to be able to define the list precisely.
	\end{itemize}
\end{itemize}

\newpage
\printbibliography
\end{document}