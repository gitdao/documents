# Git DAO

Open Source Software creates $500bn/year in economic value, yet OSS maintainers are often underpaid, overworked, and under-appreciated.

## TODO

- Build a community VERY early

## Feature Ideas

ICO for early open source projects

## Related Newsletter

- The DAO Revolution: https://newsletter.banklesshq.com/p/daos-are-the-opportunity-of-a-lifetime
- Future of Work: https://newsletter.banklesshq.com/p/the-future-of-work
- Coordination Failures: https://newsletter.banklesshq.com/p/know-thy-enemy-coordination-failures
- The Ultimate DAO Report: https://newsletter.banklesshq.com/p/the-ultimate-dao-report
- How to Launch a DAO: https://newsletter.banklesshq.com/p/how-to-launch-a-dao

## Resources

- Twitter - 10 Lessons from DAO land for the new year: https://twitter.com/ShapeShiftCOO/status/1477461168801280004

## Competitors

- Gitcoin and Quadratic Funding: https://wtfisqf.com/?grant=&grant=&grant=&grant=&match=1000
- FundOSS: https://fundoss.org
- Radicle: https://radicle.xyz/, Radicle is a peer-to-peer stack for building software together.
- Opolis: https://opolis.co/, Opolis celebrates your independence while providing the employee benefits you need. 
- Coordinape: https://coordinape.com/, Scale your community with tools to reward contributors, incentivize participation and manage resources
- SourceCred: https://sourcecred.io/docs, A tool for communities to measure and reward value creation.
- Coordination.Party Kit: https://gov.gitcoin.co/t/coordination-party-kit/9231, a tool suite that helps recruit, recognize, reward, and retain contributors to DAOs.
- Aragon: aragon.org

## Open Source Governance Models

- Python: https://discuss.python.org/t/comparison-of-the-7-governance-peps/392, https://medium.com/@herveberaud.pro/python-after-guido-bdfl-the-future-of-the-governance-of-python-2969bab19c7e
- Rust: 10 teams
- Linux: https://wiki.p2pfoundation.net/Linux_-_Governance#Linux_Organization

## Why it Matters

You can work anonymously.
This implies no more discriminations based on:

- age, so you can work the same as others even if you are 16 or 85 years old
- gender, e.g. motherhood penalty
- skin color
- religion

Geographical borders don't matter anymore as everything is digital.
No need to worry about work permits or visas.

Promote and pay people based on their works quality and nothing else.

## Coordination Failures

- Nation-state refusing to give up on nuclear weapons even though they present an existential threat
- Consumers refusing to give up on fossil fuel vehicles although they present an existential threat
- Individual cells of the human body live harmoniously pooling their resources for the greater good of the organism; but if a cell defects from this equilibrium, it becomes cancerous—eventually outcompeting all the other cells and taking over the body.

Coordination failures are dictator-less dystopias, situations that every single citizen including the leadership hates but which nevertheless endures unconquered.
From a god's-eye view, there is a clear problem with coordination failures; from within the system, no one actor can create change, so the best option is to continue ignoring the problem.

Coordination failures have also been called multi-polar traps. A multi-polar trap goes something like this:

1. Individual humans want to breathe clean air.
1. There is work to be done to prevent pollution from having a high ppm (parts per million) in the air we breathe.
1. Since clean air is non rivalrous (my consuming it does not stop you from consuming it) and non excludable (one cannot put clean air behind a velvet rope and charge for it) this means that each individual actor has a rational incentive to free ride on the system.
   Why contribute back to a fund to maintain clean air if I already get it for free.
   This is the first trap: the single player trap.
1. If enough actors choose to free ride, the entire system begins to bear a burden, and if that burden increases to a point of systemic collapse, no one gets these public goods.
   This is the second trap: the multiplayer trap.
1. From a god's-eye perspective, the obvious solution is for everyone to chip in a little bit—but without a coordination mechanism to ensure everyone does contribute, they do not.

When players in the game perceive common identity or purpose with one another, or if they have the negative externalities of their behavior priced into market-based decisions they make, they play the game together instead of separately—and avoid the second trap.

## Trust Game

https://ncase.me/trust/

The conclusion we can draw from game theory about trust:

- For trust to appear, you need repeated interactions
- For trust to appear, you need a non-zero-sum game, i.e. the game must make it possible for both parties to benefit from their interactions.
- Low miscommunication: If the level of miscommunication is too high, trust breaks down.
  But when there's a little bit of miscommunication, it pays to be more forgiving.

The biggest lesson: What the game is, defines what the players do.
Our problem today isn't just that people are losing trust, it's that our environment acts against the evolution of trust.
That may seem cynical or naive -- that we're "merely" products of our environment -- but as game theory reminds us, we are each others' environment. In the short run, the game defines the players. But in the long run, it's us players who define the game.
