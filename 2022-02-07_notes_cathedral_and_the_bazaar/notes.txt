Every good work of software starts by scratching a
developer's personal itch.

Necessity is the mother of invention

Good programmers know what to write. Great ones know what to
rewrite (and reuse).


you get an A not for effort but for results, 

Stay or switch? If I switched, I'd be throwing away the coding
I'd already done in exchange for a better development base.


Plan to throw one away; you will, anyhow." (Fred Brooks, The
Mythical Man-Month, Chapter 11)



 Or, to put it another way, you often don't really understand the
problem until after the first time you implement a solution.

. If you have the right attitude, interesting problems will
find you.


When you lose interest in a program, your last duty to it is
to hand it off to a competent successor.


Users are wonderful things to have, and not
just because they demonstrate that you're serving a need, that
you've done something right. Properly cultivated, they can become
co-developers.


Treating your users as co-developers is your least-hassle
route to rapid code improvement and effective debugging.


Release Early, Release Often


Most developers (including me) used to believe
this was bad policy for larger than trivial projects, because early
versions are almost by definition buggy versions and you don't want
to wear out the patience of your users.


Linus was treating his users as co-developers in the most
effective possible way:
 7. Release early. Release often. And listen to your
customers.


Linus was keeping his
hacker/users constantly stimulated and rewarded-stimulated by the
prospect of having an ego-satisfying piece of the action,

Given a large enough beta-tester and co-developer base,
almost every problem will be characterized quickly and the fix
obvious to someone.


Given enough eyeballs, all bugs are
shallow." I dub this: "Linus's Law".


In the bazaar view, on the other hand, you assume that bugs are
generally shallow phenomena-or, at least, that they turn shallow
pretty quickly when exposed to a thousand eager co-developers
pounding on every single new release.

that the averaged opinion of a
mass of equally expert (or equally ignorant) observers is quite a
bit more reliable a predictor than the opinion of a single
randomly-chosen one of the observers. They called this the Delphi
effect. 

contributions are received not from a random sample, but from
people who are interested enough to use the software, learn about
how it works, attempt to find solutions to problems they encounter,
and actually produce an apparently reasonable fix. Anyone who
passes all these filters is highly likely to have something useful
to contribute.


Debugging is parallelizable".
Although debugging requires debuggers to communicate with some
coordinating developer, 

maintaining a widely used program is typically 40 percent or more
of the cost of developing it. 

last version designated
"stable" or to ride the cutting edge and risk bugs in order to get
new features. This tactic is not yet systematically imitated by
most Linux hackers, but perhaps it should be; the fact that either
choice is available makes both more attractive. 

the kind of bug report non-source-aware users normally turn in
tends not to be very useful. Non-source-aware users tend to report
only surface symptoms; they take their environment for granted, so
they (a) omit critical background data, and (b) seldom include a
reliable recipe for reproducing the bug.


Thus, source-code awareness by both parties greatly enhances
both good communication and the synergy between what a beta-tester
reports and what the core developer(s) know. In turn, this means
that the core developers' time tends to be well conserved, even
with many collaborators.


Brooks's Law
predicts that the complexity and communication costs of a project
rise with the square of the number of developers, while work done
only rises linearly.


communications/coordination overhead on a project tends to
rise with the number of interfaces between human beings. 

problems scale with the number of communications paths between
developers, which scales as the square of the humber of developers

on
open-source projects, the halo developers work on what are in
effect separable parallel subtasks and interact with each other
very little; code changes and bug reports stream through the core
group, and only within that small core group do we pay the full
Brooksian overhead. 

Smart data structures and dumb code works a lot better than
the other way around.


I released early and often
(almost never less often than every ten days; during periods of
intense development, once a day).
 I grew my beta list by adding to it everyone who contacted me
about fetchmail. I sent chatty announcements to the beta list
whenever I released, encouraging people to participate. And

The next best thing to having good ideas is recognizing good
ideas from your users. Sometimes the latter is better.


Often, the most striking and innovative solutions come from
realizing that your concept of the problem was wrong.


Perfection (in design) is achieved not when there is
nothing more to add, but rather when there is nothing more to take
away.

When your development mode is
rapidly iterative, development and enhancement may become special
cases of debugging-fixing 'bugs of omission' in the original
capabilities or concept of the software.


Consider the way a puddle of water finds a
drain, or better yet how ants find food: exploration essentially by
diffusion, followed by exploitation mediated by a scalable
communication mechanism. 

MIME (Multipurpose Internet Mail Extensions) operation.

one cannot code from the ground up in
bazaar style [IN].

it would be very hard to originate a project in bazaar mode.


Your nascent developer
community needs to have something runnable and testable to play
with.


When you start community-building, what you need to be able to
present is a plausible promise. Your program doesn't have to work
particularly well. It can be crude, buggy, incomplete, and poorly
documented. What it must not fail to do is (a) run, and (b)
convince potential

developers that it can be evolved into
something really neat in the foreseeable

Linux and fetchmail both went public with strong, attractive
basic designs. Many people thinking about the bazaar model as I
have presented it have correctly considered this critical,


 I think it is not critical that the coordinator be able to
originate designs of exceptional brilliance, but it is absolutely
critical that the coordinator be able to recognize good design
ideas from others.

So I believe the fetchmail project succeeded partly because I
restrained my tendency to be clever; this argues (at least) against
design originality being 

. A
bazaar project coordinator or leader must have good people and
communications skills.


To solve an interesting problem, start by finding a problem
that is interesting to you.


programmer
time is not fungible; adding developers to a late software project
makes it later. 

The developer who uses only his
or her own brain in a closed project is going to fall behind the
developer who knows how to 

create an open, evolutionary context in
which feedback exploring the design space, code contributions,
bug-spotting, and other improvements come from from hundreds
(perhaps thousands) of people.


Another vital factor was the development of a leadership
style and set of cooperative customs that could allow developers to
attract co-developers and get maximum leverage out of the
medium.


The "severe effort of many converging wills" is precisely what a
project like Linux requires-and the "principle of command" is
effectively impossible to apply among volunteers in the anarchist's
paradise we call the Internet.

hackers who want to lead collaborative projects have to learn how
to recruit and energize effective communities of interest

The Linux world behaves in many respects like a free
market or an ecology, a collection of selfish agents attempting to
maximize

utility which in the process produces a self-correcting
spontaneous order more elaborate and efficient than any amount of
central planning could have achieved.

The "utility function" Linux hackers are maximizing is not
classically economic, but is the intangible of their own ego
satisfaction and reputation among other hackers. (One may call
their motivation "altruistic", but this ignores the fact that
altruism is itself a form of ego satisfaction for the altruist).


We may view Linus's method as a way to create an efficient
market in "egoboo"-to connect the selfishness of individual hackers
as firmly as possible to difficult ends that can only be achieved
by sustained cooperation. 

Both the fetchmail and Linux kernel projects show that by
properly rewarding the egos of many other hackers, a strong
developer/coordinator can use the Internet to capture the benefits
of having lots of co-developers without having a project collapse
into a chaotic mess. So

Provided the development coordinator has a communications
medium at 

as good as the Internet, and knows how to lead
without coercion, many heads are inevitably better than one.


the future of open-source software will increasingly
belong to people who know how to play Linus's game, people who
leave behind the cathedral and embrace the bazaar. This is not to
say that individual vision and brilliance will no longer matter;
rather, I think that the cutting edge of open-source software will
belong to people who start from individual vision and brilliance,
then

 amplify it through the effective construction of voluntary
communities of interest.
 Perhaps

evolutionary arms race with open-source communities that can put
orders of magnitude more skilled time into

, I have
developed the idea that expected future service value is the key to
the economics of software production in the essay The Magic
Cauldron.


there have been open-source projects
that maintained a coherent direction and an 

effective maintainer
community over quite long periods of time without the kinds of
incentive structures or institutional controls that conventional
management finds essential.

what, if anything, the tremendous
overhead of conventionally-managed development is actually buying
us.
 Whatever it is certainly doesn't include reliable execution by
deadline, or on budget, or to all features of the specification;
it's a rare 'managed' project that meets even one of these goals,
let alone all three.

One thing many people think the traditional mode buys you is
somebody to hold legally liable and potentially recover
compensation from if the project goes wrong. But this is an
illusion; most

software licenses are written to disclaim even
warranty of merchantability, let alone performance-and cases of
successful recovery for software nonperformance are vanishingly
rare.

Even if they were common, feeling comforted by having
somebody to sue would be missing the point. You didn't want to be
in a lawsuit; you wanted working software.


project management has five
functions:
 To define goals and keep everybody pointed in the same direction
To monitor and make sure crucial details don't get skipped To
motivate people to do boring but necessary drudgework To organize
the deployment of people for best productivity To marshal resources
needed to sustain the project

under the open-source
model, and in

its surrounding social context, they can begin to
seem strangely irrelevant. We'll take them in reverse order.


a lot of resource marshalling is
basically defensive; once you have your people and machines and
office space, you have to defend them

The volunteer ethos tends to take care
of the 'attack' side of resource-marshalling automatically; people
bring their own resources to the table.

in a world of cheap PCs and fast Internet links, we find
pretty consistently that the only really limiting resource is
skilled attention.

Open-source projects, 

for want of machines or links or office
space; they die only when the developers themselves lose


open source has been
successful partly because its culture only accepts the most
talented 5% or

She spends most of
her time organizing the 

deployment of the other 95%, 

The success of the open-source community sharpens this question
considerably, by providing hard evidence that it is often cheaper
and more effective to recruit self-selected volunteers from the
Internet than it is to manage buildings full of people who would
rather be doing something else.


a claim that the open-source
community can only be relied on only to do work that is 'sexy' or
technically sweet; anything else will be left undone (or done only
poorly) unless it's churned out by money-motivated cubicle peons
with managers cracking whips over them. I address the psychological
and social reasons for being skeptical of this claim in
Homesteading the Noosphere.

If the conventional, closed-source, heavily-managed style of
software development is really defended only by a sort of Maginot
Line of problems conducive to boredom, then it's going to remain
viable in each individual application area for only so long as
nobody finds those problems really interesting and nobody else
finds any way to route around them. Because the moment there is
open-source competition for a 'boring' piece of software, customers
are going to know that

it was finally tackled by someone who chose
that problem to solve because of a fascination with the problem
itself-which, in software as in other kinds of creative work, is a
far more effective motivator than money

the strongest argument the open-source community has is that
decentralized peer review trumps all the conventional methods for
trying to ensure that details don't get

Our creative play has been racking up technical, market-share, and
mind-share successes at an astounding rate. We're proving not only
that we can do better software, but that joy is an

Human beings generally take pleasure in a task
when it falls in a sort of optimal-challenge zone; not so easy as
to be boring, not too hard to achieve. A happy programmer is one
who is neither underutilized nor weighed down with ill-formulated
goals and stressful process friction. Enjoyment predicts

It may well turn out that one of the most important effects of
open source's success will be to teach us that play is the most
economically efficient mode of creative work.

The combination of Linus's Law and Hasler's Law suggests that
there are actually three critical size regimes in software
projects. On small projects (I would say one to at most three
developers) no management structure more elaborate than picking a
lead programmer is needed. And there is some intermediate range
above that in which the cost of 

traditional management is
relatively low, so its benefits from avoiding duplication of
effort, bug-tracking, and pushing to see that details are not
overlooked actually net out positive.
 Above that, however, the combination of Linus's Law and Hasler's
Law suggests there is a large-project range in which the costs and
problems of traditional management rise much faster than the
expected cost from duplication of effort. Not the least of these
costs is a structural inability to harness the many-eyeballs
effect, which (as we've seen) seems to do a much better job than
traditional

management at making sure bugs and details are not
overlooked. Thus, in the large-project case, the combination of
these laws effectively drives the net payoff of traditional
management to

When programmers are held both to an immutable feature list and a
fixed drop-dead date, quality goes out the window and there is
likely a colossal mess in the making. I am indebted to Marco
Iansiti

and Alan MacCormack of the Harvard Business School for
showing me me evidence that relaxing either one of these
constraints can make scheduling workable.
 One way to do this is to fix the deadline but leave the feature
list flexible, allowing features to drop off if not completed by
deadline. This is essentially the strategy of the "stable" kernel
branch; Alan Cox (the stable-kernel maintainer) puts out releases
at fairly regular intervals, but makes no guarantees about when

particular bugs will be fixed or what features will beback-ported
from the experimental branch.
 The other way to do this is to set a desired feature list and
deliver only when it is done. This is essentially the strategy of
the "experimental" kernel branch. De Marco and Lister cited
research showing that this scheduling policy ("wake me up when it's
done") produces not only the highest quality but, on average,
shorter delivery times than either "realistic" or "aggressive"
scheduling.

severely underestimated the importance of
the "wake me up when it's done" anti-deadline policy to the
open-source community's productivity and quality.

It may well turn out to be that the process transparency of open
source is one of three co-equal drivers of its quality, along with
"wake me up when it's done" scheduling and developer
self-selection.


It's tempting, and not entirely inaccurate, to see the
core-plus-halo organization characteristic of open-source projects
as an Internet-enabled spin on Brooks's own recommendation for
solving the N-squared complexity problem,

the open-source culture
leans heavily on strong

Unix traditions of modularity, APIs, and
information hiding-

The respondent who pointed out to me the effect of widely
varying trace path lengths on the difficulty of characterizing a
bug speculated that trace-path difficulty for multiple symptoms of
the same bug varies "exponentially" (which I take to mean on a
Gaussian or Poisson distribution, and agree seems very plausible).
If it is experimentally possible to get a handle on the shape of
this distribution, that would be extremely valuable data. Large
departures from a flat equal-probability

distribution of trace
difficulty would suggest that even solo developers should emulate
the bazaar strategy by bounding the time they spend on tracing a
given symptom before they switch to another. Persistence may not
always be a virtue…


An issue related to whether one can start projects from
zero in the bazaar style is whether the bazaar style is capable of
supporting truly innovative work.

But there is a more fundamental error in the implicit assumption
that the cathedral model (or the bazaar model, or any other kind of
management structure) can somehow make innovation happen reliably.
This is nonsense.

Insight comes from
individuals. The most their surrounding social machinery can ever
hope to do is to be responsive to breakthrough insights-to nourish
and reward and rigorously test them instead of squashing them.


Rather I am pointing out that every such group
development starts from-is necessarily sparked by-one good idea in
one person's head. Cathedrals and bazaars and other social
structures can catch that lightning and refine it, but they cannot
make it on 

SNAFU
Principle': "True communication is possible only between equals,
because inferiors are more consistently rewarded for telling their
superiors pleasant lies than for telling the truth." 

Creative
teamwork utterly depends on true communication and is thus very
seriously hindered by the presence of power relationships. The
open-source community, effectively free of such power
relationships,

the SNAFU principle predicts in authoritarian
organizations a progressive disconnect between decision-makers and
reality, as more and more of the input to those who decide tends to
become pleasant lies. 

