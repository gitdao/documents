import xml.etree.ElementTree as ET

def getNotesText(xmlfile):
    """
    Parses all the notes taken in an epub files and returns them as a string.
    """
  
    tree = ET.parse(xmlfile)
    root = tree.getroot()
  
    notes = []

    for item in root.findall(
        './{http://ns.adobe.com/digitaleditions/annotations}annotation/' +
        '{http://ns.adobe.com/digitaleditions/annotations}target/' +
        '{http://ns.adobe.com/digitaleditions/annotations}fragment'
    ):
        for child in item:
            notes.append(child.text + '\n\n')

    return notes

      
def main():

    notes = getNotesText('notes_cathedral_and_bazaar.epub.annot')

    with open("notes.txt", 'w') as file:
        file.writelines(notes)


if __name__ == '__main__':
    main()
