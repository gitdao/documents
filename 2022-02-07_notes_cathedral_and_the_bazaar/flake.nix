{
  description = "Notes on The Cathedral and the Bazaar";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, devshell, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: 
      let

        pkgs = import nixpkgs {
          config.allowUnfree = true;
          overlays = [ devshell.overlay ];
          inherit system;
        };

        packageName = "parse-xml-annotations";

        app = pkgs.poetry2nix.mkPoetryApplication {
          projectDir = ./.;
        };
      in {

        packages.${packageName} = app;

        defaultPackage = self.packages.${system}.${packageName};

        devShell = # Used by `nix develop`, value needs to be a derivation
          pkgs.mkShell {
            inputsFrom = builtins.attrValues self.packages.${system};
        };
      }
  );
}
