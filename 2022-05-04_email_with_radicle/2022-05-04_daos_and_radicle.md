# Convince Radicle to Use DAOs for More

Radicle version 0.2.12 uses DAOs as a way to keep an official version of the project.

I believe that DAOs can be used for (a lot) more.
Assume that most projects use a DAO.
The following becomes possible:

- DAOs can still be used as a way to define which commit is the "official" version of the project.
- Using Drips, a percentage of the donations received by any project can be distributed to other Open Source projects the current project relies on (financing model for Open Source, i.e. Drips at large scale!).
  This creates some kind of donation graph, which ensures all Open Source projects receive some money instead of only the most visible ones as is the case now.
- Contributors of the project are rewarded with governance tokens, i.e. with power over the project.
- A percentage of the received donations is redistributed to developers proportionally to their power inside the DAO (salaries to contributors of a project).

Integrating blockchain a little tighter into Radicle brings many advantages:
- Propose a "business model" for Open Source projects (this has been a problem for decades now!).
- Use governance token as a reputation mechanism for developers.
- Propose a very decentralized governance processes by default (voting system), and move away from the Benevolent Dictator For Life model —think Linus Torvald— which could bring a lot of value to society as a whole.

Cons:

- Tighter integration between Radicle and blockchains.
  Need to think about whether it is acceptable to require all users to have a blockchain account to participate.

